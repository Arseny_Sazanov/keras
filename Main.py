# СВОИ БИБЛИОТЕКИ
from Lib import LIB, commentjson as CJ
import IDS     # Ident. Datasets
import Models  # Keras model generator
import PPR as PPR
import re
# ОБЩЕНАУЧНЫЕ БИБЛИОТЕКИ
import numpy as np

# import os
# import matplotlib as mpl
# if os.environ.get('DISPLAY','') == '':
#     print('No display found. Using non-interactive Agg backend')
#     mpl.use('Agg')
# else:
#     print("DISPLAY SETTINGS: %s" % os.environ.get('DISPLAY', ''))

import matplotlib.pyplot as plt

# %matplotlib inline   # for Jupyter Notebook

# НЕЙРОСЕТЕВЫЕ БИБЛИОТЕКИ
import keras
from keras import backend as BackendKeras
from keras.callbacks import EarlyStopping
from keras.callbacks import TensorBoard
from keras.optimizers import SGD

# PYTHON БИБЛИОТЕКИ
import copy
import argparse

# ГЛОБАЛЬНЫЕ ПЕРЕМЕННЫЕ

Logger = LIB.Logger    # Логгер
CLogger = LIB.CLogger  # Цветной логгер


# Парсер аргументов командной строки
def create_parser():
    pars = argparse.ArgumentParser()
    pars.add_argument('-v', '--verbose', action='store_const', help="Debug mod (On/Off)", const=True, default=False)      # Debug mod
    pars.add_argument('-s', '--settings', help="Path to setting json file", default="./settings.json")                    # Путь к файлу общих настроек
    pars.add_argument('-e', '--experiments', help="Path to experiments json file", default="exp_diff_K.json")             # Путь к файлу настроек экспериментов
    pars.add_argument('-es', '--early_stopping', action='store_const', help="Early Stopping", const=True, default=False)  # Включить колбэк ранний останов
    pars.add_argument('-tb', '--tensorboard', action='store_const', help="Tensor Board", const=True, default=False)       # Включить колбэк Доска Тензорборд, WEB GUI TensorFlow
    pars.add_argument('-dp', '--debug_plots', action='store_const', help="Debug Plots", const=True, default=False)        # Отладочные графики
    pars.add_argument('-hp', '--history_plots', action='store_const', help="History Plots", const=True, default=False)    # Графики о History обучения
    pars.add_argument('-fp', '--final_plots', action='store_const', help="Final Plots", const=True, default=False)        # Финальные графики
    return pars


# НАЧАЛО MAIN ПРОГРАММЫ
if __name__ == "__main__":

    # Работа с аргументами командной строки
    parser = create_parser()
    namespace = parser.parse_args()  # ==== namespace = parser.parse_args(sys.argv[1:])

    if not namespace:
        CLogger.critical("Can't parse namespace!")
        exit(1)

    CLogger.print("Verbose mod is %s" % namespace.verbose)
    CLogger.print("Settings file path: %s" % namespace.settings)
    CLogger.print("Settings example file: %s" % namespace.experiments)
    CLogger.print("Debug plot option: %s" % namespace.debug_plots)
    CLogger.print("Final plot option: %s" % namespace.final_plots)

    CLogger.config_logger(debug_info_on=namespace.verbose)

    CLogger.info("\nStart script!\n")

    # 0. ЭКСПОРТ НАСТРОЕК СКРИПТА ИЗ JSON файлов

    SETTINGS = {}  # Словарь настроек
    EXPERIMENTS = []  # Массив настроек экспериментов
    CONFIGURATION_FILES_PATH = "./json/"
    MODELS_PATH = "/models/"
    EXPERIMENTS_PATH = "/experiments/"

    # 0.1. Загрузка общих базовых настроек
    try:
        with open(CONFIGURATION_FILES_PATH + namespace.settings) as settings_file:
            SETTINGS = CJ.load(settings_file)
    except Exception as inst:
        Logger.fatal("Can't open file with base settings. %s" % inst)
        exit(1)

    # 0.2. Загрузка настроек модели
    try:
        with open("{path}/{model_path}/{model}.json".format(
                path=CONFIGURATION_FILES_PATH,
                model_path=MODELS_PATH,
                model=SETTINGS["NAME_MODEL"])) as model_file:
            SETTINGS["MODEL"] = CJ.load(model_file)
    except Exception as inst:
        Logger.fatal("Can't open file with model settings. %s" % inst)
        exit(2)

    # 0.3. Загрузка параметров экспериментов
    try:
        with open("{path}/{exp_path}/{exp_file}".format(
                path=CONFIGURATION_FILES_PATH,
                exp_path=EXPERIMENTS_PATH,
                exp_file=namespace.experiments)) as exp_file:
            EXPERIMENTS = CJ.load(exp_file)
    except Exception as inst:
        Logger.fatal("Can't open file with experiments. %s" % inst)
        exit(3)

    CLogger.debug(SETTINGS)
    CLogger.debug(EXPERIMENTS)

    RESULTS = {
        'Min_T': [], 'D_T': [],
        'Good_percent': [], 'Fail_percent': [],
        'NN_Train_percent': [], 'NN_Test_percent': []
        }

    #  ЦИКЛ ЭКСПЕРИМЕНТОВ
    for i, E in enumerate(EXPERIMENTS):

        CLogger.info("=================================")
        CLogger.info("Experiment %s starts!" % i)
        CLogger.info("Settings experiment: %s" % E)

        CLogger.debug("Step 1. Update settings")

        # Настройки эксперимента
        #   1.1. Базовые настройки
        S = copy.deepcopy(SETTINGS)      # Берем базовые настройки

        #   1.2. Подмена базовых настроек на настройки экспериментов
        for key in E:    # для каждой настройки эксперимента
            # if key in S.keys():   # Если настройка есть в базовых настрйоках  TODO подумать над этиим местом
            S[key] = E[key]  # Подмена базовых настреок на текущие

        #  1.3. Вычисление автовычисляемых настроек
        S["K"] = S["K_TRAIN"] + S["K_TEST"]
        S["N"] = S["T"] - S["M"]

        CLogger.debug(S)

        # 2. Генерация данных
        CLogger.debug("Step 2. Update settings")
        results_IDS = IDS.generate_new_dataset(S, namespace)
        (x_train, y_train) = results_IDS["train"]
        (x_validate, y_validate) = results_IDS["validate"]
        (x_test, y_test) = results_IDS["test"]
        place_def_train = results_IDS["place_def_train"]
        place_def_validate = results_IDS["place_def_validate"]
        place_def_test = results_IDS["place_def_test"]

        # reshape for Keras Models
        if re.match("FFNN", S["NAME_MODEL"]):
            x_train = x_train.transpose()  # or  x_train.reshape(x_train.shape[1], x_train.shape[0])
            x_test = x_test.transpose()
        if re.match("1D_CNN", S["NAME_MODEL"]):
            if BackendKeras.image_data_format() == 'channels_first':
                x_test = x_test.reshape(x_test.shape[1], 1, S["M"])
                x_train = x_train.reshape(x_train.shape[1], 1, S["M"])
                S["MODEL"]["input_shape"] = (1, S["M"])
            else:
                x_train = x_train.reshape(x_train.shape[1], S["M"], 1)
                x_test = x_test.reshape(x_test.shape[1], S["M"], 1)
                S["MODEL"]["input_shape"] = (S["M"], 1)
        if re.match("CNN", S["NAME_MODEL"]):
            rows = S["MODEL"]["ROWS"]
            cols = S["MODEL"]["COLS"]
            if BackendKeras.image_data_format() == 'channels_first':
                x_test = x_test.reshape(x_test.shape[2], 1, rows, cols)
                x_train = x_train.reshape(x_train.shape[2], 1, rows, cols)
                S["MODEL"]["input_shape"] = (1, rows, cols)
            else:
                x_train = x_train.reshape(x_train.shape[2], rows, cols, 1)
                x_test = x_test.reshape(x_test.shape[2], rows, cols, 1)
                S["MODEL"]["input_shape"] = (rows, cols, 1)

        # 3. Генерация моделей Keras
        CLogger.debug("Step 3. Generate Keras Model")
        #   3.1. Вызов генератора моделей
        model = Models.generate_model(NameModel=S["NAME_MODEL"], MS=S["MODEL"])  # Функция в зависимости от наcтроек сгенерировать модель Keras

        #   3.2. Компиляция модели

        # initiate RMSprop optimizer
        #opt = keras.optimizers.rmsprop(lr=0.0001, decay=1e-6)

        opt=S["MODEL"]["optimizer"]
        if S["MODEL"]["optimizer"] == "sgd":
            lrate = S["MODEL"]["optimizer_param"]["lrate"]
            momentum = S["MODEL"]["optimizer_param"]["momentum"]
            decay = lrate / S["MODEL"]["epochs"]
            opt = SGD(lr=lrate, momentum=momentum, decay=decay, nesterov=False)
        elif S["MODEL"]["optimizer"] == "rmsprop":
            pass
        model.compile(loss=S["MODEL"]["loss"], optimizer=opt, metrics=["%s" % S["MODEL"]["metrics"]])

        #   3.3. Вывод информации по модели
        CLogger.info(model.summary())

        # 4. Обучение  модели Keras
        CLogger.debug("Step 4. Learning Keras Model")

        #   4.1. Prepare
        # Convert class vectors to binary class matrices
        # For example it makes:
        # 0 - > [1, 0]
        # 1 - > [0, 1]
        y_train_cat = keras.utils.to_categorical(y_train, S["NumberOfClass"])
        y_test_cat = keras.utils.to_categorical(y_test, S["NumberOfClass"])

        # Список функций callback, для регламентации характера обучения
        callbacks = []
        # Ранний останов, если результаты не улучшаются
        early_stopping = EarlyStopping(monitor='val_loss', patience=S["MODEL"]["early_stop_patience"])
        tensorboard = TensorBoard(
            log_dir='./TensorBoardGraph',  # Файлы журналов ссохраняютс в этом каталоге
            histogram_freq=1,  # Запись гистограммы активации в каждой эпохе
            embeddings_freq=1,  # Запись векторных представлений в каждой эпохе
            write_graph=True,
            write_images=True
        )

        if namespace.early_stopping:
            callbacks.append(early_stopping)

        if namespace.tensorboard:
            callbacks.append(tensorboard)
            # NB! Usage tensorboard --logdir path_to_current_dir/TensorBoardGraph

        #   4.2. Learning
        history = model.fit(x_train, y_train_cat,
                  batch_size=S["MODEL"]["batch_size"],
                  epochs=S["MODEL"]["epochs"],
                  validation_split=S["MODEL"]["validation_split"],
                  verbose=S["MODEL"]["verbose"],
                  callbacks=callbacks)

        #   4.3 Графики обучения

        if namespace.history_plots:
            acc = history.history['acc']
            val_acc = history.history['val_acc']
            loss = history.history['loss']
            val_loss = history.history['val_loss']

            epochs = range(1, len(acc) + 1)

            # "bo" is for "blue dot"
            plt.plot(epochs, loss, 'bo', label='Training loss')
            # b is for "solid blue line"
            plt.plot(epochs, val_loss, 'b', label='Validation loss')
            plt.title('Training and validation loss')
            plt.xlabel('Epochs')
            plt.ylabel('Loss')
            plt.legend()
            plt.show()

        # 5. Результаты обучения
        CLogger.debug("Step 5. Calculating result of learning")

        #   5.1. Оцениваем качество обучения сети на обучающих данных
        scores_train = model.evaluate(x_train, y_train_cat, verbose=0)
        CLogger.info("Точность работы на обучаемых данных: %.2f%%" % (scores_train[1] * 100))

        #   5.2. Оцениваем качество обучения сети на тестовых данных
        scores_test = model.evaluate(x_test, y_test_cat, verbose=0)
        CLogger.info("Точность работы на тестовых данных: %.2f%%" % (scores_test[1] * 100))

        #   5.2. Запускаем сеть на входных данных
        predictions = model.predict(x_test)

        #   5.2. Преобразуем входные данные сети из категорий в метки классов (цифры от 0 - 9)
        predictions_n = np.argmax(predictions, axis=1)

        # 6. Пост обработка данных от модели
        CLogger.debug("Step 6. Analyze result of learning")
        # Пост обработка: "Оценка скорости обнаружения дефекта (на каком такте) на РОДИТЕЛЬСКОЙ выборке

        # расчет вероятности ложного обнаружения и другие характеристики
        RESULTS = PPR.postprocessing_results(S, x_test, y_test, place_def_test, predictions_n, RESULTS)

        # 7. Графики выходов слоев

        if namespace.debug_plots:
            pass
            # from keras.models import Model
            #
            # layer_1_out = Model(
            #     inputs=model.input,
            #     outputs=model.get_layer("conv1d_1").output).predict(x_train[0].reshape(1, S["M"], 1))
            #
            # layer_2_out = Model(
            #     inputs=model.input,
            #     outputs=model.get_layer("conv1d_2").output).predict(x_train[0].reshape(1, S["M"], 1))
            #
            # plt.figure()
            # plt.plot(np.arange(0, S["M"], 1), x_train[0], label="input")
            # for i in np.arange(0, 32, 1):
            #     plt.plot(np.arange(0, S["M"], 1), layer_1_out[0, :, i], label="conv_1_out_%s" % i)
            # plt.grid()
            # plt.legend()
            #
            # plt.figure()
            # plt.plot(np.arange(0, S["M"], 1), x_train[0], label="input")
            # for i in np.arange(0, 32, 1):
            #     plt.plot(np.arange(0, S["M"], 1), layer_2_out[0, :, i], label="conv_2_out_%s" % i)
            # plt.grid()
            # plt.legend()

            # plt.show()

###########################

            # from keras import backend as K
            #
            # # with a Sequential model
            # get_layers = K.function([model.layers[0].input, K.learning_phase()],
            #                         [model.layers[0].output, model.layers[1].output,
            #                          model.layers[2].output, model.layers[3].output,
            #                          model.layers[4].output, model.layers[5].output,
            #                          model.layers[6].output, model.layers[7].output])
            # W = x_train[46]
            #
            # # test_mod = 0
            # layers_output = get_layers([W.reshape(1, S["M"], 1), 0])
            # # # train_mod = 1
            # # layers_output = get_layers([W.reshape(1, S["M"], 1), 1])
            #
            # plt.figure()
            # plt.title("1st Convulaion layer output")
            # plt.plot(np.arange(0, S["M"], 1), W, label="input")
            # w, h, d = layers_output[0].shape
            # plt.plot(np.arange(0, h, 1), layers_output[0][0, :, :])
            # plt.grid()
            # plt.legend()
            #
            # plt.figure()
            # plt.title("2nd Convulaion layer output")
            # plt.plot(np.arange(0, S["M"], 1), W, label="input")
            # w, h, d = layers_output[1].shape
            # plt.plot(np.arange(0, h, 1), layers_output[1][0, :, :])
            # plt.grid()
            # plt.legend()
            #
            # plt.figure()
            # plt.title("MaxPooling layer output")
            # for i in [2, 3]:
            #     w, h, d = layers_output[i].shape
            #     plt.plot(np.arange(0, h, 1), layers_output[i][0, :, :])
            #
            # plt.grid()
            # plt.legend()
            #
            # plt.figure()
            # plt.title("Flatten layer output")
            # for i in [4]:
            #     w, h = layers_output[i].shape
            #     plt.plot(np.arange(0, h, 1), layers_output[i][0, :], label="layer_%s_out" % i)
            # plt.grid()
            # plt.legend()
            #
            # plt.figure()
            # plt.title("Dense layer output")
            # for i in [5, 6]:
            #     w, h = layers_output[i].shape
            #     plt.plot(np.arange(0, h, 1), layers_output[i][0, :], label="layer_%s_out" % i)
            # plt.grid()
            # plt.legend()
            #
            # plt.figure()
            # plt.title("Output classification")
            # w, h = layers_output[7].shape
            # plt.bar(np.arange(h), layers_output[7][0, :])
            # plt.xticks(np.arange(h), ["NO defect", "Defect"])
            # plt.legend()
            #
            # plt.show()

        CLogger.info("Experiment %s finished!" % i)

    CLogger.info("All Experiments finished!")

    CLogger.info("\nRESULTS:")
    CLogger.info(RESULTS)

    # 7. Построение графиков результатов

    if namespace.final_plots:  # если не выключена графики
        # Графики результатов экспериментов

        fig1 = plt.figure()
        plt.plot(range(len(EXPERIMENTS)), RESULTS['Min_T'], label='Min_T', marker='o', ls=":")
        plt.xticks(range(len(EXPERIMENTS)), [e for e in EXPERIMENTS])
        plt.xlabel('Experiments')
        plt.ylabel('Tics')
        plt.legend()

        fig2 = plt.figure()
        plt.plot(range(len(EXPERIMENTS)), RESULTS['D_T'], label='D_T', marker='o', ls=":")
        plt.xticks(range(len(EXPERIMENTS)), [e for e in EXPERIMENTS])
        plt.xlabel('Experiments')
        plt.ylabel('Tics')
        plt.legend()

        fig3 = plt.figure()
        plt.plot(range(len(EXPERIMENTS)), RESULTS['Good_percent'], label='Good_percent', marker='o', ls=":")
        plt.plot(range(len(EXPERIMENTS)), RESULTS['Fail_percent'], label='Fail_percent', marker='o', ls=":")
        plt.xticks(range(len(EXPERIMENTS)), [e for e in EXPERIMENTS])
        plt.xlabel('Experiments')
        plt.ylabel('%')
        plt.legend()

        # fig4 = plt.figure()
        # plt.plot(range(len(EXPERIMENTS)), RESULTS['NN_Train_percent'], label='NN_Train_percent', marker='o', ls=":")
        # plt.plot(range(len(EXPERIMENTS)), RESULTS['NN_Test_percent'], label='NN_Test_percent', marker='o', ls=":")
        # plt.xticks(range(len(EXPERIMENTS)), [e for e in EXPERIMENTS])
        # plt.xlabel('Experiments')
        # plt.ylabel('%')
        # plt.legend()

        plt.show()

    CLogger.info("Finish script successful! :) ")

    # TODO
    #   1. Сохранение моделей в файлы
    #   3. Сохранение картинок графиков
    #   4.