import numpy as np
from Lib import LIB
import matplotlib.pyplot as plt
import re
CLogger = LIB.CLogger  # Цветной логгер


# Генерация тестовой и обучающей выборки для задачи идентификации разладки случайного процесса
def generate_new_dataset(S, namespace):
    '''
    :param S:
        Settings for generate function
        M, D, N, K, T and other
    :param namespace:
        Flags ettings of program
    :return:
        Dict of results
    '''

    try:
        SEED = S["Seed"]
        M0 = S["M0"]
        D0 = S["D0"]
        T = S["T"]
        K_train = S["K_TRAIN"]
        K_validate = S["K_VALIDATE"]
        K_test = S["K_TEST"]

        NumberOfClass = S["NumberOfClass"]  # пока = 2 TODO
        D_F_TR = S["MACRO_P_DEFECT_TRAIN"]
        M_D_TR = S["M_DEFECT_TRAIN"]
        D_D_TR = S["D_DEFECT_TRAIN"]
        t_min_train = S["T_MIN_DEFECT_TRAIN"]
        t_max_train = S["T_MAX_DEFECT_TRAIN"]

        D_F_TE = S["MACRO_P_DEFECT_TEST"]
        M_D_TE = S["M_DEFECT_TEST"]
        D_D_TE = S["D_DEFECT_TEST"]
        t_min_test = S["T_MIN_DEFECT_TEST"]
        t_max_test = S["T_MAX_DEFECT_TEST"]

    except Exception as inst:
        LIB.CLogger.error("!!! Not found some param !!!")
        raise(inst)

    # Установка "зерна" для генератора случайных чисел
    np.random.seed(SEED)
    # Генерация матриц cлучайных последовательностей
    # Нормальное функционирование
    R0_train = np.random.normal(M0, D0, (T, K_train))  # Без дефекта. Тренировочное. Нормальное распределение Гаусса-Лапласа
    R0_validate = np.random.normal(M0, D0, (T, K_validate))  # Без дефекта. Тренировочное Валидация. Нормальное распределение Гаусса-Лапласа
    R0_test = np.random.normal(M0, D0, (T, K_test))  # Без дефекта. Тестовое. Нормальное распределение Гаусса-Лапласа
    # Функционирование с дефектом
    R_def_train = np.random.normal(M_D_TR, D_D_TR, (T, K_train))  # С дефектом. Нормальное распределение Гаусса-Лапласа
    R_def_validate = np.random.normal(M_D_TR, D_D_TR, (T, K_validate))  # С дефектом. Валидация. Нормальное распределение Гаусса-Лапласа
    R_def_test = np.random.normal(M_D_TE, D_D_TE, (T, K_test))  # С дефектом. Нормальное распределение Гаусса-Лапласа
    # Факт дефекта
    D_fact_train = np.random.choice([0, 1], p=[1-D_F_TR, D_F_TR], size=K_train)  # Вектор фактов дефекта учебная
    D_fact_validate = np.random.choice([0, 1], p=[1-D_F_TR, D_F_TR], size=K_validate)  # Вектор фактов дефекта валидация
    D_fact_test = np.random.choice([0, 1], p=[1-D_F_TE, D_F_TE], size=K_test)    # Вектор факта дефекта тестовая
    # Места дефектов
    T_def_train = np.random.randint(t_min_train, t_max_train, K_train)  # Место дефекта обучаающая выборка
    T_def_validate = np.random.randint(t_min_train, t_max_train, K_validate)  # Место дефекта валидационная выборка
    T_def_test = np.random.randint(t_min_test, t_max_test, K_test)  # Место дефекта тестовая выборка

    # Проверка М.О. и дисперсии
    if namespace.verbose:
        CLogger.print("TRAIN          Mean:%s, Std:%s" % (R0_train.mean(), R0_train.std()))
        CLogger.print("VALIDATE       Mean:%s, Std:%s" % (R0_validate.mean(), R0_validate.std()))
        CLogger.print("TEST           Mean:%s, Std:%s" % (R0_test.mean(), R0_test.std()))
        CLogger.print("TRAIN_DEF      Mean:%s, Std:%s" % (R_def_train.mean(), R_def_train.std()))
        CLogger.print("VALIDATE_DEF   Mean:%s, Std:%s" % (R_def_validate.mean(), R_def_validate.std()))
        CLogger.print("TEST_DEF       Mean:%s, Std:%s" % (R_def_test.mean(), R_def_test.std()))
    # Для генерации дефектов
    # Def_happen = np.random.randint(0, NumberOfClass, K_test)  # Факт дефекта
    # Class_def = np.random.randint(0, NumberOfClass, K_test)  # Класс дефекта (0 нет, 1 есть дефект1, 2 дефект 2 и т.д.) пока NumberOfClass = 2

    # Уравнение авторегрессия второго порядка
    # Xn = c + a1 * Xn-1 + a2*Xn-2 + En  (En - шум случ. сост. с зада M и D)

    result = {}
    result["train"] = base_generator(S=S, namespace=namespace, K=K_train, R0=R0_train, R_D=R_def_train, T_D=T_def_train, D_F=D_fact_train)
    result["validate"] = base_generator(S=S, namespace=namespace, K=K_validate, R0=R0_validate, R_D=R_def_validate, T_D=T_def_validate, D_F=D_fact_validate)
    result["test"] = base_generator(S=S, namespace=namespace, K=K_test, R0=R0_test, R_D=R_def_test, T_D=T_def_test, D_F=D_fact_test)
    result["place_def_train"] = T_def_train
    result["place_def_validate"] = T_def_validate
    result["place_def_test"] =  T_def_test

    return result


def base_generator(S, namespace, K, R0, R_D, T_D, D_F):
    '''
    BASE GENERATOR FUNCTION
    :param S: Settings dict
    :param namespace: flags of program
    :param K: Number macro seqenses
    :param R0: Normal process without defect
    :param R_D: Defect normal process
    :param T_D: Time defect
    :return: x and y samples for NN
    '''

    NAME_MODEL = S["NAME_MODEL"]
    a1 = S["A1"]
    a2 = S["A2"]
    c0 = S["C0"]

    T = S["T"]
    N = S["N"]  # T-M
    M = S["M"]

    # Вспомогательный массив для МАКРО последовательностей                                       [ 0 ............... T]
    temp = np.zeros((T, K)).astype('float32')
    # Выходной массив выборок, окошки по M значений полученных из МАКРО последовательности [0,M] [1,M+1] ... [T-M, T] - N штук
    x = None
    cols = rows = 0
    if re.match("FFNN", S["NAME_MODEL"]) or re.match("1D_CNN", S["NAME_MODEL"]):
        x = np.zeros((M, K * N), dtype='float32')
    elif re.match("CNN", S["NAME_MODEL"]):
        rows = S["MODEL"]["ROWS"]
        cols = S["MODEL"]["COLS"]
        # проверка от дурака
        if rows * cols != M:
            LIB.CLogger.error("Bad param!!! Row * Col != M")
            raise (Exception("Bad param!"))
        x = np.zeros((rows, cols, K * N), dtype='float32')
    else:
        pass

    # Выходной вектор ответа учителя , (есть дефект, нет дефекта)
    y = np.zeros(K * N).astype('int')

    LIB.CLogger.info("Begin Generate Sample")
    LIB.CLogger.debug("Params: T: %s; N: %s; M: %s" % (T, N, M))
    status_bar = LIB.ProcessingCounter("[GENERATE SAMPLES] it is processing...",
                                       max_cnt_value=K * N,
                                       every_cnt_percent=10,
                                       silence=False)
    # Статистика по генерации
    n_macro_def = 0
    n_macro_no_def = 0

    n_window_def = 0
    n_window_no_def = 0

    # Генерация обучающей выборки
    for j in range(K):
        # LIB.Logger.debug(T_D[j])
        defect_generate = False
        # Место дефекта кодирует и факт дефекта, если место дефекта >= T --> t_def = T т.е. нет дефекта, или сейчас не дефект условие из вектора
        T_D[j] = T_D[j] if T_D[j] < T and D_F[j] == 1 else T  # перезапись места дефекта на самый конец, т.е. для НЕ генерации дефектной части выборки
        t_def = T_D[j]
        # Генерация участка макропоследовательности без дефекта [0:t_def)
        for i in range(0, t_def):
            temp[i][j] = c0 + R0[i][j]
            if i > 0:
                temp[i][j] += a1 * temp[i - 1][j]
            if i > 1:
                temp[i][j] += a2 * temp[i - 2][j]
            if namespace.debug_plots:
                plt.scatter(i, temp[i][j], edgecolors='b', s=10)

        # Генерация участка макропоследовательности с дефектом [t_def:T] or if t_def >= T --> nothing to do
        for i in range(t_def, T):
            defect_generate = True
            # Выбор дефекта в зависимости от выборки
            temp[i][j] = c0 + R_D[i][j]
            if i > 0:
                temp[i][j] += a1 * temp[i - 1][j]
            if i > 1:
                temp[i][j] += a2 * temp[i - 2][j]
            # plt.scatter(i, temp[i][j], edgecolors='r', s=10)

        n_macro_def += 1 if defect_generate else 0
        n_macro_no_def += 1 if not defect_generate else 0

        if namespace.debug_plots:
            plt.plot(np.arange(0, S["T"], 1), temp[:, j], label="Time series sample. M:%s D:%s" % (S["M0"], S["D0"]))
            plt.xticks(np.arange(0, S["T"], 1))
            if defect_generate:
                plt.plot([t_def, t_def], [min(temp[:, j]), max(temp[:, j])], 'r--',
                         label="Time defect: %s. M_def: %s  D_def: %s" % (t_def, S["M_DEFECT_TRAIN"], S["D_DEFECT_TRAIN"]))
            plt.legend()

            plt.figure()
        ## plt.show()

        if re.match("FFNN", S["NAME_MODEL"]) or re.match("1D_CNN", S["NAME_MODEL"]):
            # fig2 = plt.figure()
            # Генерация окошек из эксперимента [0:M], [1:M+1]  e.t.c.
            for k in range(0, N):  # N = T-M
                status_bar.increment_counter()  # инкремент счетчик статус бара
                v = N * j + k
                x[:, v] = temp[k:M + k, j]
                y[v] = 1 if M + k > t_def else 0

                n_window_def += 1 if M + k >= t_def else 0  # инкремент числа окон с дефектом
                n_window_no_def += 1 if M + k < t_def else 0  # инкремент числа оконо без дефекта

            if namespace.debug_plots:
                plt.plot(np.arange(0, S["M"], 1), x[:, v], label='№ окна %s, [%s:%s], Дефект: %s' % (v, k, M+k, y[v]))
                plt.legend()
                plt.xticks(np.arange(0, S["M"], 1))
                plt.show()

        elif re.match("CNN", S["NAME_MODEL"]):
            for k in range(0, N):  # N = T-M - сколько нарезать окошек из макро последовательности
                status_bar.increment_counter()  # инкремент счетчик статус бара

                v = N * j + k  # текущий номер окошка
                y[v] = 1 if M + k >= t_def else 0

                n_window_def += 1 if M + k >= t_def else 0    # инкремент числа окон с дефектом
                n_window_no_def += 1 if M + k < t_def else 0  # инкремент числа оконо без дефекта

                # заполнение змейкой 1-2-3
                #                    6-5-4
                #                    7-8-9
                for i_k in range(cols):
                    x[i_k, :, v] = temp[k:M + k, j][i_k*rows:i_k*rows+rows] if i_k % 2 == 0 else temp[k:M + k, j][i_k*rows:i_k*rows+rows][::-1]
                # [::-1] -  reverse() - обратный порядок
        else:  # other model to future
            pass

    LIB.CLogger.debug("\nGenerate Macro samples: \nWith defect: %s\nWithout: %s\nTotal: %s\n"
                      % (n_macro_def, n_macro_no_def, n_macro_def+n_macro_no_def))
    LIB.CLogger.debug("\nFrom Macro samples Generate Windows:\nWith defect: %s\nWithout: %s\nTotal: %s\n"
                      % (n_window_def, n_window_no_def, n_window_def+n_window_no_def))
    LIB.CLogger.info("End Generate Sample")

    # Транспонирование
    # x = x.transpose()
    # y = y.transpose()

    return x, y
