import numpy
from keras.datasets import mnist
from keras.models import Sequential
from keras.layers import Dense
from keras.utils import np_utils

# Устанавливаем seed для повторяемости результатов
numpy.random.seed(42)

# Загружаем данные
(X_train, y_train), (X_test, y_test) = mnist.load_data()

# Преобразование размерности изображений
X_train = X_train.reshape(60000, 784)
X_test = X_test.reshape(10000, 784)
# Нормализация данных
X_train = X_train.astype('float32')
X_test = X_test.astype('float32')
X_train /= 255
X_test /= 255

# Преобразуем метки в категории
Y_train = np_utils.to_categorical(y_train, 10)
Y_test = np_utils.to_categorical(y_test, 10)

# 0 - > [1, 0, 0, 0, 0, 0, 0 ,0 ,0 ,0]
# 1 - > [0, 1, 0, 0, 0, 0, 0 ,0 ,0 ,0]
# 2 - 9 etc.

# Создаем последовательную модель
model = Sequential()

# Добавляем уровни сети
model.add(Dense(800, input_dim=784, activation="relu", kernel_initializer="normal"))
model.add(Dense(10, activation="softmax", kernel_initializer="normal"))

# Компилируем модель
model.compile(loss="categorical_crossentropy", optimizer="SGD", metrics=["accuracy"])

print(model.summary())

# Обучаем сеть
model.fit(X_train, Y_train, batch_size=200, epochs=1, validation_split=0.2, verbose=2)

# Оцениваем качество обучения сети на тестовых данных
scores = model.evaluate(X_test, Y_test, verbose=0)
print("Точность работы на тестовых данных: %.2f%%" % (scores[1]*100))


# Запускаем сеть на входных данных
predictions = model.predict(X_train)

# Преобразуем входные данные сети из категорий в метки классов (цифры от 0 - 9)
temp = numpy.argmax(predictions, axis=1)

print(temp)

# USING model auto learning
# serialize model to JSON
model_json = model.to_json()
with open("./models/model.json", "w") as json_file:
    json_file.write(model_json)

# serialize weights to HDF5
model.save_weights("./models/model.h5")
print("Saved model to disk")

# OR

# from keras.models import load_model
#
# model.save('./models/my_model.h5')  # creates a HDF5 file 'my_model.h5'
# del model  # deletes the existing model
#
# # returns a compiled model
# # identical to the previous one
# model = load_model('./models/my_model.h5')