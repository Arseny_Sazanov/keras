def get_first_index(value, array):
    for i, v in enumerate(array):
        if v == value:
            return i
    return 0
