import json
import re
# Модуль поддержки json с комментариями


# Загрузка из файла
# 1. Игнорируем строки:
#     а) в начале которых есть   ignore_match     r'^[ ,    \n]*//+'
#     б) в которых есть          ignore_serch     r'---+'
# 2. Вырезаем из строк которые не попали в п.1 все после
#                                split_zero_after  "//"
def load(file, ignore_match=r'^[ ,    \n]*//+', ignore_search=r'---+', split_zero_after="//"):
    clean_json = ''.join(line.split(split_zero_after)[0] for line in file if not (re.match(ignore_match, line) or re.search(ignore_search, line)))
    return json.loads(clean_json)