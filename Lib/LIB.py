import sys
import time
# import pymysql
import datetime
# from datetime import timezone
import logging
import json
# import xml.etree.cElementTree as etree
# import psycopg2
# import redis

# ОПИСАНИЕ
# Базовая вспомогательная "библиотека" для написания скриптов
# Содержит основные классы для работы с БД MYSQL и дочерние классы для работы с конкретными БД логов и статистики
# Содержит реализацию:
#   Логгера
#   Профилировщика времени
#   Цветовые схемы
#   Локер (pid file)


# Класс Цвета ANSII
class Color:
     # Escape sequence	Text attributes
     Off           ="\x1b[0m"    # All attributes off(color at startup)
     Bold          ="\x1b[1m"    # Bold on(enable foreground intensity)
     Underline     ="\x1b[4m"    # Underline on
     Blink         ="\x1b[5m"    # Blink on(enable background intensity)
     Bold_off      ="\x1b[21m"   # Bold off(disable foreground intensity)
     Underline_off ="\x1b[24m"   # Underline off
     Blink_off     ="\x1b[25m"   # Blink off(disable background intensity)

     Black         = "\x1b[30m"	 # Black
     Red           = "\x1b[31m"	 # Red
     Green         = "\x1b[32m"	 # Green
     Yellow        = "\x1b[33m"	 # Yellow
     Blue          = "\x1b[34m"	 # Blue
     Magenta       = "\x1b[35m"	 # Magenta
     Cyan          = "\x1b[36m"	 # Cyan
     White         = "\x1b[37m"	 # White
     Default       = "\x1b[39m"	 # Default(foreground color at startup)
     Light_Gray    = "\x1b[90m"	 # Light Gray
     Light_Red     = "\x1b[91m"	 # Light Red
     Light_Green   = "\x1b[92m"	 # Light Green
     Light_Yellow  = "\x1b[93m"	 # Light Yellow
     Light_Blue    = "\x1b[94m"	 # Light Blue
     Light_Magenta = "\x1b[95m"	 # Light Magenta
     Light_Cyan    = "\x1b[96m"	 # Light Cyan
     Light_White   = "\x1b[97m"	 # Light White
     Reset         = "\x1b[0m"

     # "\x1b[40m"   # Black
     # "\x1b[41m"   # Red
     # "\x1b[42m"   # Green
     # "\x1b[43m"   # Yellow
     # "\x1b[44m"   # Blue
     # "\x1b[45m"   # Magenta
     # "\x1b[46m"   # Cyan
     # "\x1b[47m"   # White
     # "\x1b[49m"   # Default(background color at startup)
     # "\x1b[100m"  # Light Gray
     # "\x1b[101m"  # Light Red
     # "\x1b[102m"  # Light Green
     # "\x1b[103m"  # Light Yellow
     # "\x1b[104m"  # Light Blue
     # "\x1b[105m"  # Light Magenta
     # "\x1b[106m"  # Light Cyan
     # "\x1b[107m"  # Light White


COLOR = Color()


# Логирование

# Filter for Logger
class LessThanFilter(logging.Filter):
    def __init__(self, exclusive_maximum, name=""):
        super(LessThanFilter, self).__init__(name)
        self.max_level = exclusive_maximum

    def filter(self, record):
        #non-zero return means we log this message
        return 1 if record.levelno < self.max_level else 0


Verbose = True

#Get the root logger
Logger = logging.getLogger()

#Have to set the root logger level, it defaults to logging.WARNING
Logger.setLevel(logging.NOTSET)

logging_handler_out = logging.StreamHandler(sys.stdout)
logging_handler_out.setLevel(logging.DEBUG)
logging_handler_out.addFilter(LessThanFilter(logging.WARNING))
Logger.addHandler(logging_handler_out)

logging_handler_err = logging.StreamHandler(sys.stderr)
logging_handler_err.setLevel(logging.WARNING)
Logger.addHandler(logging_handler_err)


# Цветной логгер и возможность отключения debug сообщений
class ColorLogger:
    Reset = "\x1b[0m"

    def __init__(self, logger, info_color=COLOR.Green, print_color=COLOR.Blue, debug_color=COLOR.Yellow, error_color=COLOR.Red):
        self.Logger = logger
        self.infoColor = info_color
        self.printColor = print_color
        self.debugColor = debug_color
        self.errorColor = error_color
        self.debug_info_on = True

    def config_logger(self, debug_info_on=True, info_color=COLOR.Green, print_color=COLOR.Blue, debug_color=COLOR.Yellow, error_color=COLOR.Red):
        self.infoColor = info_color
        self.printColor = print_color
        self.debugColor = debug_color
        self.errorColor = error_color
        self.debug_info_on = debug_info_on

    def info(self, text):
        self.Logger.info("{c}{t}{r}".format(c=self.infoColor, r=self.Reset, t=text))

    def print(self, text):
        self.Logger.info("{c}{t}{r}".format(c=self.printColor, r=self.Reset, t=text))

    def debug(self, text):
        if self.debug_info_on:
            self.Logger.debug("{c}{t}{r}".format(c=self.debugColor, r=self.Reset, t=text))

    def error(self, text):
        self.Logger.error("{c}{t}{r}".format(c=self.errorColor, r=self.Reset, t=text))

    def fatal(self, text):
        self.Logger.fatal("{c}{t}{r}".format(c=self.errorColor, r=self.Reset, t=text))


CLogger = ColorLogger(Logger)


# Локер для недопущения повторного запуска скрипта
class Locker:
    # 1. Проверить есть ли файл.
    # 1.1 Прочитать его (получить PID), проверить есть ли процесс с этим PID.
    # 1.2 Если есть - значит один экземпляр скрипта запущен, и мы завершаемся.
    # 2. Если нет - создаем файл,
    # 2.1 пишем в него PID Текущего процесса
    # 1.2 выполняем скрипт.
    # 3. Удаляем файл.
    path_lock_file = None
    name_script = None

    def __init__(self, path_lock_file, name_script=None):
        self.path_lock_file = path_lock_file
        self.name_script = name_script

    def __enter__(self):
        self.lock()

    def __exit__(self, type, value, traceback):
        self.unlock()

    def lock(self, timedelta_before_unlock=datetime.timedelta(hours=1)):
        from os import getpid
        try:
            with open(self.path_lock_file, 'r', encoding='utf-8') as lock_file:
                # info = lock_file.read().split(' ')
                # print(info[2], info[3])
                # print(os.system("ps -ax | grep %s"%(info[3])))
                json_data = json.load(lock_file)
                date_lock_file_create = datetime.datetime.strptime(json_data["Date"], "%Y-%m-%d %H:%M:%S")
                if datetime.datetime.now() - date_lock_file_create < timedelta_before_unlock:
                    Logger.error("Another script already work! Exit(1)")
                    Logger.error("Lock file created at %s, less than %s!" % (date_lock_file_create, timedelta_before_unlock))
                    exit(1)
            self.unlock()
            raise FileNotFoundError
        except FileNotFoundError:
            try:
                with open(self.path_lock_file, 'w', encoding='utf-8') as lock_file:
                    json.dump({
                        "Script_name": self.name_script,
                        "Pid": getpid(),
                        "Date": datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")}, lock_file)
            except Exception as inst1:
                Logger.exception(inst1)

    def unlock(self):
        from os import remove
        try:
            remove(self.path_lock_file)
        except FileNotFoundError:
            Logger.warning("Warning! File not found: %s ")
        except OSError:
            Logger.error("Error OS!")
        except Exception as unknown_inst:
            Logger.error("Unknown Error" % unknown_inst)
            Logger.exception(unknown_inst)
            pass


# Класс для создания строки времени выполнения скрипта
class ProcessingCounter:
    silence = False
    info_str = "It's a counter"
    counter = 1
    one_percent = 1
    max_cnt_value = 100
    percents = 1
    every_cnt_percent = 1

    def __init__(self, info_str, max_cnt_value, every_cnt_percent=1, silence=False):
        try:
            self.info_str = info_str
            self.silence = silence
            self.max_cnt_value = max_cnt_value
            self.counter = 0
            self.percents = 0
            self.one_percent = round(max_cnt_value / 100)
            self.every_cnt_percent = every_cnt_percent
        except Exception as inst:
            Logger.exception(inst)

    def increment_counter_n(self, n):
        self.counter = self.counter + n - 1
        self.increment_counter()

    def increment_counter(self):
        self.counter += 1
        self.percents = self.counter // self.one_percent

        if (not self.silence) and (self.counter % (self.one_percent * self.every_cnt_percent) == 0):
            self.__say()

    def __say(self):
        Logger.info("%s. Percent:%s%%. (Counter = %s)" % (self.info_str, self.percents, self.counter))


# Профайлер времени выполнения участка скрипта
class Profiler:
    startTime = 0
    info_str = ""
    on = False

    def __init__(self, on, info_str):
        self.info_str = info_str
        self.on = on

    def __enter__(self):
        if self.on:
            self._startTime = time.time()

    def __exit__(self, type, value, traceback):
        if self.on:
            Logger.info("%s.Time execute: %5.3f sec" % (self.info_str, time.time() - self._startTime))

#
# # Класс для работы с БД MySQL
# class DbMysqlConnect:
#     db_connection = None
#     db_host = None
#     db_port = None
#     db_user = None
#     db_pass = None
#     db_name = None
#     autocommit = None
#
#     # Конструктор
#     def __init__(self,
#                  db_config,
#                  auto_commit=True,
#                  charset=None):
#         self.db_host = db_config["host"]
#         self.db_port = db_config["port"]
#         self.db_user = db_config["user"]
#         self.db_pass = db_config["pass"]
#         self.db_name = db_config["name_db"]
#         self.autocommit = auto_commit
#         self.charset = charset
#
#
#     # Открыть содинение с БД
#     def db_connect(self):
#         cnt = 0
#         while cnt < 10:
#             try:
#                 cnt += 1
#                 self.db_connection = pymysql.connect(host=self.db_host,
#                                                      port=int(self.db_port),
#                                                      user=self.db_user,
#                                                      passwd=self.db_pass,
#                                                      db=self.db_name,
#                                                      autocommit=self.autocommit)
#                 break
#             except Exception as inst111:
#                 Logger.error("Can't connect to database. Attempt %s/100" % cnt)
#                 Logger.exception(inst111)
#                 time.sleep(10)
#         if cnt == 10:
#             Logger.error("{c]Can't connect to database{r} ( Attemp == 10 (10*10 sec) ). Exit. ".
#                           format(c=Color.Red, r=Color.Reset))
#             return False
#
#         Logger.info("{c}Connect to DB successful! {r} \n {c2} Settings: {h}:{p} {u}:{ps} db:{d} {r}".
#                      format(h=self.db_host, p=self.db_port, u=self.db_user, ps=self.db_pass, d=self.db_name,
#                             c=Color.Green, r=Color.Reset, c2=Color.Yellow))
#
#         if self.charset:
#             Logger.info("Try set charset to %s" % self.charset)
#             try:
#                 self.db_connection.set_charset(self.charset)
#                 with self.db_connection.cursor() as cursor:
#                     cursor.execute('SET NAMES utf8;')
#                     cursor.execute('SET CHARACTER SET utf8;')
#                     cursor.execute('SET character_set_connection=utf8;')
#             except Exception as inst:
#                 Logger.error("{c]Can't change char_set to database{r} Exit. Exc: {i}".
#                              format(c=Color.Red, i=inst, r=Color.Reset))
#                 return False
#
#         return True
#
#     # Закрыть соединение с БД
#     def cleanup(self):
#         self.db_connection.close()
#
#     # Настройка типа char_set коннекшена
#     def set_char_set(self, charset=None):
#         if charset:
#             self.set_char_set(charset)
#         else:
#             self.set_char_set(self.charset)
#
#
#
# class RedisConn:
#     config = {}
#
#     redis_connect_attempts = 5
#     redis_connected = False
#     redis_client = None
#
#     def __init__(self, config):
#         self.config = config
#         self.redis_connect_attempts = 5
#         self.redis_connected = False
#         self.redis_client = None
#
#     def __enter__(self):
#         self.connect_redis()
#         return self
#
#     def __exit__(self, exc_type, exc_val, exc_tb):
#         self.cleanup_redis()
#
#     def connect_redis(self):
#         if not self.redis_connected:
#             for attempt in range(self.redis_connect_attempts):
#                 try:
#                     self.redis_client = redis.StrictRedis(host=self.config["Host"],
#                                                           port=self.config["Port"],
#                                                           db=self.config["DB"],
#                                                           password=self.config["Password"],
#                                                           decode_responses=True)
#                     self.redis_connected = True
#                     break
#                 except Exception as e:
#                     print("Can't connect to redis %s" % self.config["url"])
#                     time.sleep(0.1)
#
#     def cleanup_redis(self):
#         if self.redis_connected:
#             self.redis_client.connection_pool.disconnect()
#             self.redis_connected = False
#
#     def reconnect(self):
#         self.cleanup_redis()
#         self.connect_redis()
#
#
# # Класс для работы с БД Postgres
# class DbPostgresConnect:
#     db_connection = None
#     db_config = None
#     autocommit = None
#
#     # Конструктор
#     def __init__(self,
#                  db_config,
#                  auto_commit=True,
#                  charset=None):
#         self.db_config = db_config
#         self.autocommit = auto_commit
#         self.charset = charset
#
#
#     # Открыть содинение с БД
#     def db_connect(self):
#         cnt = 0
#         while cnt < 10:
#             try:
#                 cnt += 1
#                 self.db_connection = psycopg2.connect(**self.db_config)
#                 self.db_connection.autocommit = self.autocommit
#                 break
#             except Exception as inst111:
#                 Logger.error("Can't connect to database. Attempt %s/100" % cnt)
#                 Logger.exception(inst111)
#                 time.sleep(10)
#         if cnt == 10:
#             Logger.error("{c]Can't connect to database{r} ( Attemp == 10 (10*10 sec) ). Exit. ".
#                           format(c=Color.Red, r=Color.Reset))
#             return False
#
#         Logger.info("{c}Connect to DB successful! {r} \n {c2} Settings: {h}:{p} {u}:{ps} db:{d} {r}".
#                      format(h=self.db_config["host"], p=self.db_config["port"], u=self.db_config["user"], ps=self.db_config["password"], d=self.db_config["database"],
#                             c=Color.Green, r=Color.Reset, c2=Color.Yellow))
#
#         if self.charset:
#             Logger.info("Try set charset to %s" % self.charset)
#             try:
#                 self.db_connection.set_charset(self.charset)
#                 with self.db_connection.cursor() as cursor:
#                     cursor.execute('SET NAMES utf8;')
#                     cursor.execute('SET CHARACTER SET utf8;')
#                     cursor.execute('SET character_set_connection=utf8;')
#             except Exception as inst:
#                 Logger.error("{c]Can't change char_set to database{r} Exit. Exc: {i}".
#                              format(c=Color.Red, i=inst, r=Color.Reset))
#                 return False
#
#         return True
#
#     # Закрыть соединение с БД
#     def cleanup(self):
#         self.db_connection.close()
#
#     # Настройка типа char_set коннекшена
#     def set_char_set(self, charset=None):
#         if charset:
#             self.set_char_set(charset)
#         else:
#             self.set_char_set(self.charset)
#
#     # Выполнить запрос с возращаемыми данными
#     def execute_query(self, sql):
#         result = []
#         try:
#             with self.db_connection.cursor() as cursor:
#                 Logger.debug("SQL: \n {c} {q} {r}".format(c=Color.Cyan, q=sql, r=Color.Reset))
#                 try:
#                     cursor.execute(sql)
#                     for r in cursor.fetchall():
#                         result.append(r)
#                     return result
#                 except Exception as inst:
#                     Logger.error("{c} Error while execute_query() work with cursor."
#                                  "Exception:{exc}. DB_NAME:{name_db} SQL:{SQL} {r}"
#                                  .format(exc=inst, name_db=self.db_config["database"], SQL=sql, c=Color.Cyan, r=Color.Reset))
#         except Exception as inst1:
#             Logger.error("{c} Error while execute_query()(). Exception:{exc} \n DB_NAME:{name_db} \n SQL:{SQL} {r}"
#                          .format(c=Color.Cyan, exc=inst1,name_db=self.db_config["database"], SQL=sql, r=Color.Reset))
#             Logger.info("Try reconnect to DB")
#             self.db_connect()
#
#         return None
#
#     # Запрос без возращаемых данных
#     def execute(self, sql):
#         try:
#             with self.db_connection.cursor() as cursor:
#                 Logger.debug("SQL: \n {c} {q} {r}".format(c=Color.Cyan, q=sql, r=Color.Reset))
#                 try:
#                     cursor.execute(sql)
#                 except Exception as inst:
#                     Logger.error("{c} Error while execute() work with cursor."
#                                  "Exception:{exc}. DB_NAME:{name_db} SQL:{SQL} {r}"
#                                  .format(exc=inst, name_db=self.db_config["database"], SQL=sql, c=Color.Cyan,
#                                          r=Color.Reset))
#         except Exception as inst1:
#             Logger.error("{c} Error while execute()(). Exception:{exc} \n DB_NAME:{name_db} \n SQL:{SQL} {r}"
#                          .format(c=Color.Cyan, exc=inst1, name_db=self.db_config["database"], SQL=sql, r=Color.Reset))
#             Logger.info("Try reconnect to DB")
#             self.db_connect()
