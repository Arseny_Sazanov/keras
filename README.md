# Введение
Экспериментальный репозиторий для изучения Keras (on Tenzorflow backend)

# Задача исследования

Разработка НС для распознвоания момента дефекта в случайной последовательности

Исследуются:

* Сеть прямого распространнеия (FFNN)
* Cверточная сеть (CNN)

# Установка требуемых зависимостей для проекта

## Установка virtualenvwrapper

https://virtualenvwrapper.readthedocs.io/en/latest/install.html

Перед этим нужно установить virtualenv

    sudo pip3 install pip --upgrade
    sudo pip3 install virtualenv
    sudp pip3 install virtualenvwrapper 

В ~/.bashrc

    mcedit ~/.bashrc && 
    
 прописать:

    export VIRTUALENVWRAPPERPYTHON=/usr/local/bin/python3.6  # Путь к Python для Virtualenv
    export WORKON_HOME=$HOME/Env                             # Папка где хранятся окружения Virtualenv
    export VIRTUALENVWRAPPER_SCRIPT=/home/arseny/.local/bin/virtualenvwrapper.sh  # путь к sh скрипту враппера which virtualenvwrapper.sh
    source ~/.local/bin/virtualenvwrapper_lazy.s

Создание окружения c именем keras:

    mkvirtualenv --no-site-packages keras

Удаление окружения:

    rmvirtualenv keras

Открытие  окружения:

    workon keras
    
Закрытие окружения:
    
    deactivate

## Скачивание исходных кодов

    git clone https://Arseny_Sazanov@bitbucket.org/Arseny_Sazanov/test_keras.git

Прим. должен быть установлен git
    
    sudo apt-get install git
    
## Установка требуемых пакетов и зависимостей из файла rec.txt(библиотеки Python)

    workon keras    
    pip3 install -r rec.txt
    
Прим. наоборот сохранить зависимости

    pip3 freeze > rec.txt

## Настройка отображения графиков библиотеки MATPLOTLIB на удаленном серверер

Коннект должен быть с флагом **-X**. Пример:   
    
Если работаем из под Pycharm IDE, самым простой способ:
1) Запуск паралельной консоли с командой
    
    
    ssh -X arseny@home.esta.spb.ru -i ~/.ssh/big_server

2) Установить переменную окружения **DISPLAY** в PyCharm run configuration:


    export DISPLAY=localhost:10.0

![Alt text](https://esta.spb.ru/images/bitbucket_doc/test_keras_settings_main_remote_display.png "Pycharm Project Configuration")


    ssh -X arseny@home.esta.spb.ru -i ~/.ssh/big_server

    echo $DISPLAY
    localhost:10.0

## Запуск скрипта

Происходит стандартно:

    cd {YOUR PATH TO TEST_KERAS FOLDER HERE}/test_keras/
    python3.6 Main.py -v -s cnn_settings.json

    -v - флаг запуска в режиме отладки (больше информации)
    -s - флаг запуска с указанным конфигурационным файлом, по-умолчанию ./json/settings.json

================================================================================

P.S.  Ломается Ubunta 
command_not_found -- not found!

https://askubuntu.com/questions/720416/no-module-named-gdbm?utm_medium=organic&utm_source=google_rich_qa&utm_campaign=google_rich_qa