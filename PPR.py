import numpy as np
from Lib import LIB


def postprocessing_results(S, x_test, y_test, place_def_test, predictions, RESULTS):
    '''

    :param S: settings script
    :param x_test: тестовая выборка
    :param y_test:
    :param place_def_test:
    :param predictions:
    :return: dict RESULTS with Probability of experiments
    '''

    N = S["N"]
    K = S["K_TEST"]
    M = S["M"]
    T = S["T"]

    LIB.CLogger.info("Postprocessing START!")

    # y_place = t_def[K_train:]  # тестовые данные
    # y_fact = Def_happen        # тестовые данные факт дефекта

    V = (-100)*np.ones((K, N), dtype="int")  # скорость предсказания (-100 не было, +100 пропуск, >0 задержка тактов)

    # Статистика по макро выборкам
    defect_count = {}
    defect_count["zero_time"] = np.zeros(K, dtype=int)
    defect_count["late"] = np.zeros(K, dtype=int)
    defect_count["early"] = np.zeros(K, dtype=int)
    defect_count["false_defect"] = np.zeros(K, dtype=int)
    defect_count["not_found_true"] = np.zeros(K, dtype=int)
    defect_count["not_found_false"] = np.zeros(K, dtype=int)

    Err = predictions - y_test  # Подсчет процентов верных обнаруж, ошибок 1 и 2 рода в окнах

    Err1 = sum([1 if v > 0 else 0 for v in Err]) / predictions.size    # ошибка 1 рода по всем окнам
    Err2 = sum([1 if v < 0 else 0 for v in Err]) / predictions.size    # ошибка 2 рода по всем окнам
    Rigth = sum([1 if v == 0 else 0 for v in Err]) / predictions.size  # верно по всем окнам

    K_DEFECT = 0
    K_NO_DEFECT = 0

    for k in range(K):   # По всем МАКРО выборкам

        # Вычисление места дефекта и номер окна дефекта
        defect_exist = True
        t_defect = place_def_test[k]  # время дефекта в макропоследовательности
        if t_defect < T:
            K_DEFECT += 1
            w_defect = t_defect - M + 1 # N.B!!! важно нумерация окон с 0, 0 окно оканчивает на 9 самле+ (t_defect % M != 0)  # окно в котором встретился дефект в первые
        else:
            K_NO_DEFECT += 1
            defect_exist = False  # нет дефекта в макровыборке  t_def > T, дефект не был сгенерирован

        for n in range(N):  # Пробегаемя по всем окнам в рамках макропоследовательности

            i = k * N + n         # номер окна в массиве prediction,   n - номер окна в макропоследовательности, k - номер макропоследовательности

            t_begin = n           # такты начала и конца окна в рамках макропоследовательности
            t_end = t_begin + M   #

            if predictions[i] > 0:  # Сеть НАШЛА Дефект
                if defect_exist:  # ДЕФЕКТ ЕСТЬ
                    if n < w_defect:
                        # РАНЬШЕ Времени
                        defect_count["early"][k] = 1
                        defect_count["not_found_false"][k] = 0
                        defect_count["not_found_true"][k] = 0
                    elif n == w_defect:
                        # В яблочко
                        defect_count["zero_time"][k] = 1
                        defect_count["not_found_false"][k] = 0
                        defect_count["not_found_true"][k] = 0
                    else:  # n > w_defect
                        # ПОЗДНЕЕ Обнаружение
                        defect_count["late"][k] = 1
                        defect_count["not_found_false"][k] = 0
                        defect_count["not_found_true"][k] = 0
                    V[k][n] = t_end - t_defect  # вычисляем на сколько тактов она запоздала/опередила/угадала
                    break
                else:  # НЕТ ДЕФЕКТА
                    defect_count["false_defect"][k] = 1
                    defect_count["not_found_false"][k] = 0
                    defect_count["not_found_true"][k] = 0
                    break
            else:   # Сеть НЕ НАШЛА ДЕФЕКТ
                if defect_exist:
                    V[k][n] = 100  # пропуск дефекта
                    defect_count["not_found_false"][k] = 1
                else:
                    defect_count["not_found_true"][k] = 1

    # LIB.CLogger.info("K_DEFECT: %s\nK_NO_DEFECT: %s\n" % (K_DEFECT, K_NO_DEFECT))

    # LIB.CLogger.info("Массивы обнаружений\n"
    #                 "zero_time:      %s\n"
    #                 "late:      %s\n"
    #                 "early:     %s\n"
    #                 "false_defect: %s\n"
    #                 "not_found_true:  %s\n"
    #                 "not_found_false:  %s\n"
    #             % (defect_count["zero_time"], defect_count["late"], defect_count["early"],
    #                defect_count["false_defect"], defect_count["not_found_true"], defect_count["not_found_false"]))

    K_found_zero = defect_count["zero_time"].sum(axis=0)
    K_found_late = defect_count["late"].sum(axis=0)
    K_found_false_early = defect_count["early"].sum(axis=0)
    K_found_false_no_defect = defect_count["false_defect"].sum(axis=0)
    K_not_found_false = defect_count["not_found_false"].sum(axis=0)
    K_not_found_true = defect_count["not_found_true"].sum(axis=0)

    KK = {}
    T_plot = []
    V_sred = 0
    temp_k = 0
    for k in range(K):
        for n in range(0, N):
            if V[k][n] >= 0 and V[k][n] != 100:
                T_plot.append(V[k, n])
                if V[k][n] not in KK.keys():
                    KK[V[k][n]] = 1
                else:
                    KK[V[k][n]] += 1
                V_sred += V[k, n]
                temp_k += 1
                break
    V_sred = V_sred / temp_k if temp_k != 0 else 1

    T_D = 0
    T_N = 0
    for v in T_plot:
        T_N += 1
        T_D += (v - V_sred) * (v - V_sred)
    T_D /= (T_N-1) if T_N-1 != 0 else 1

    # График "ящик с усами" для анализа T min
    # plt.boxplot(T_plot)
    # plt.xticks(np.arange(len(X)) + 1, ('A', 'B', 'C', 'D'))

    LIB.CLogger.info("Macro with defect: %s\n Without defect: %s\n" % (K_DEFECT, K_NO_DEFECT))

    W = np.zeros(2).astype("int")
    W[0] = y_test.sum(axis=0)
    W[1] = y_test.size - W[0]

    LIB.CLogger.info("Window with defect:    %s" % W[0])
    LIB.CLogger.info("Window without defect: %s" % W[1])
    LIB.CLogger.info("Total cnt window:      %s" % (K*N))

    LIB.CLogger.info("\nAll windows: \n"
                "Rigth: %.2f%%\n"
                "Err1:  %.2f%%\n"
                "Err2:  %.2f%%\n" % (Rigth*100, Err1*100, Err2*100))

    # Logger.info(KK)
    temp_s = KK
    # TODO сортировать ключи для красоты
    #
    # for kk in (KK.keys()):
    #     temp_s += "%s:%s " % (kk, KK[kk])

    print(LIB.Color.Blue)
    print("-----------------")
    print("|  T  |  Count  |")
    print("-----------------")
    for key in sorted(KK.keys()):
        print("|  %s  |  %s      |" % (key, KK[key]))
    print("-----------------")
    print(LIB.Color.Reset)

    LIB.CLogger.info("K_found_zero:        %s   %.2f%%\n"
                "K_found_late:             %s   %.2f%%\n"
                "K_found_false_early:      %s   %.2f%%\n"
                "K_found_false_no_defect:  %s   %.2f%%\n"
                "K_not_found_false:        %s   %.2f%%\n"
                "K_not_found_true:         %s   %.2f%%\n"
                "\n T found defect = [%s]\n"
                % (K_found_zero, K_found_zero/K*100,
                   K_found_late, K_found_late/K*100,
                   K_found_false_early, K_found_false_early/K*100,
                   K_found_false_no_defect, K_found_false_no_defect/K*100,
                   K_not_found_false, K_not_found_false/K*100,
                   K_not_found_true, K_not_found_true/K*100,
                   temp_s))

    LIB.CLogger.info("GOOD_DETECT [Кол-во ВЕРНЫХ обнаружений/(и не обнаружений)]:       {good}/{all}   {pT}\n"
                "FALSE_DETECT [Кол-во НЕВЕРНЫХ обнаружений/(и ложных обнаружений)]: {bad}/{all}   {pF}\n"
                "MIN cnt t to detect defect [Мин кол-во тактов до правильного обнаружения]:       {v}  Дисперсия: {d}\n"
                .format(good=K_found_zero + K_found_late + K_not_found_true,
                        bad=K_found_false_early + K_found_false_no_defect + K_not_found_false,
                        all=K,
                        pT="%.2f%%" % ((K_found_zero + K_found_late + K_not_found_true) / K * 100),
                        pF="%.2f%%" % ((K_found_false_early + K_found_false_no_defect + K_not_found_false) / K * 100),
                        v="%.4f" % V_sred,
                        d="%.4f" % T_D)
                )

    RESULTS['Min_T'].append(V_sred)
    RESULTS['D_T'].append(T_D)
    RESULTS['Good_percent'].append((K_found_zero + K_found_late + K_not_found_true) / K * 100)
    RESULTS['Fail_percent'].append((K_found_false_early + K_found_false_no_defect + K_not_found_false) / K * 100)

    return RESULTS



