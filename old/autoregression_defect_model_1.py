import Lib.LIB as LIB

import pydot_ng as pydot

import numpy as np
from keras.datasets import mnist
from keras.models import Sequential
from keras.layers import Dense
from keras.utils import np_utils
from keras.utils import plot_model

# Инициализ
Logger = LIB.Logger  # Логгер
if __name__ == '__main__':
    # Устанавливаем seed для повторяемости результатов
    np.random.seed(42)

    Logger.info("START LOAD DATA FROM CSV FILES!")
    # Загружаем данные из файлов
    x_train = np.loadtxt('./training_samples/X_train.csv', delimiter=',')
    y_train = np.loadtxt('./training_samples/Y_train.csv', delimiter=',')
    x_test = np.loadtxt('./training_samples/X_test.csv', delimiter=',')
    y_test = np.loadtxt('./training_samples/Y_test.csv', delimiter=',')
    Logger.info("FINISHED LOAD DATA FROM CSV FILES!")

    # Транспонирование
    X_train = x_train.transpose()
    X_test = x_test.transpose()

    # Указание типа данных
    X_train = X_train.astype('float32')
    X_test = X_test.astype('float32')
    Y_train = y_train.astype(int)
    Y_test = y_test.astype(int)

    # Преобразуем метки в категории
    Y_train = np_utils.to_categorical(Y_train, 2)
    Y_test = np_utils.to_categorical(Y_test, 2)

    # 0 - > [1, 0]
    # 1 - > [0, 1]

    # Создаем последовательную модель
    model = Sequential()

    # Добавляем уровни сети
    model.add(Dense(10, input_dim=10, activation="relu", kernel_initializer="normal"))
    model.add(Dense(2, activation="softmax", kernel_initializer="normal"))

    # Компилируем модель
    model.compile(loss="categorical_crossentropy", optimizer="SGD", metrics=["accuracy"])

    print(model.summary())

    # Обучаем сеть
    model.fit(X_train, Y_train, batch_size=200, epochs=10, validation_split=0.2, verbose=2)

    #plot_model(model, to_file='model.png', show_shapes=True)

    # Оцениваем качество обучения сети на тестовых данных
    scores = model.evaluate(X_test, Y_test, verbose=0)
    print("Точность работы на тестовых данных: %.2f%%" % (scores[1]*100))

    # Запускаем сеть на входных данных
    predictions = model.predict(X_test)

    # Преобразуем входные данные сети из категорий в метки классов (цифры от 0 - 1)
    predictions = np.argmax(predictions, axis=1)

    # Пост обработка: "Оценка скорости обнаружения дефекта (на каком такте) на РОДИТЕЛЬСКОЙ выборке"

    T = 50   # длина родительской выборки
    M = 10   # длина подвыборки (окно кол-во нейронов)
    N = T-M  # кол-во подвыборок для родительской выборки

    Logger.info("Postprocessing START!")

    Logger.info("START LOAD DATA FROM CSV FILES!")
    # Загружаем данные из файлов
    y_place = np.loadtxt('./training_samples/Y_place_def_test.csv', delimiter=',').astype(int)
    Logger.info("FINISHED LOAD DATA FROM CSV FILES!")

    Err = []  # вспомогательный массив вдля дефектов
    Err1 = 0  # дефект не найден, а он есть
    Err2 = 0  # дефект найден, но его нет

    V = (-100)*np.ones((y_place.size, N)).astype("int")  # скорость предсказания (-1 не найден, или не было, >=0 кол-во тактов)
    K_rigth_found_def = np.zeros(y_place.size).astype("int")

    for k in range(y_place.size):
        for n in range(N):
            i = k*N + n
            Err.append(predictions[i] - y_test[i])
            # if y_test[i] <= 0:
            #     V[k][n] = -2
            # else:  # y_test[i] > 0  Появился дефект
                # y_place[k] - место дефекта
                # текущая позиция окна [n:n+M]
            if predictions[i] > 0:  # сеть нашла дефект
                V[k][n] = (M + n - y_place[k])
                if V[k][n] >= 0:
                    K_rigth_found_def[k] = 1
                #break  # останавливаем поиск
            else:
                if y_test[i] > 0:
                    V[k][n] = 100

    Err1 = sum([1 if v > 0 else 0 for v in Err]) / predictions.size  # ошибка 1 рода
    Err2 = sum([1 if v < 0 else 0 for v in Err]) / predictions.size  # ошибка 2 рода

    V_sred = 0
    temp_k = 0
    for k in range(y_place.size):
        for n in range(0, N):
            if V[k][n] >= 0 and V[k][n] != 100:
                V_sred += V[k, n]
                temp_k +=1
                break
    V_sred = V_sred / temp_k


    Logger.info("Err1: %.2f%%\nErr2: %.2f%%\n" % (Err1*100, Err2*100))
    Logger.info("V_rigth_cnt_find [кол-во верных обнаружений]: %s/%s\n"
                "V_min_sred_rigth [Мин кол-во тактов до правильного обнаружения]: %.2f\n"
                % (K_rigth_found_def.sum(axis=0), y_place.size, V_sred))
    print(V)


    # print(predictions[:25])
    # print(y_test.astype(int)[:25])
    # print(abs(y_test.astype(int)[:25]-predictions[:25]))
    # print(sum(abs(y_test.astype(int)[:]-predictions[:])))


    # # USING model auto learning
    # # serialize model to JSON
    # model_json = model.to_json()
    # with open("./models/model.json", "w") as json_file:
    #     json_file.write(model_json)
    #
    # # serialize weights to HDF5
    # model.save_weights("./models/model.h5")
    # print("Saved model to disk")
    #
    # OR
    #
    # from keras.models import load_model
    #
    # model.save('./models/my_model.h5')  # creates a HDF5 file 'my_model.h5'
    # del model  # deletes the existing model
    #
    # # returns a compiled model
    # # identical to the previous one
    # model = load_model('./models/my_model.h5')