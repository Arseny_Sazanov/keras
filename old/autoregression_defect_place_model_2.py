import numpy as np
from keras.datasets import mnist
from keras.models import Sequential
from keras.layers import Dense
from keras.utils import np_utils


if __name__ == '__main__':
    # Устанавливаем seed для повторяемости результатов
    np.random.seed(42)

    # Загружаем данные из файлов
    x_train = np.loadtxt('./training_samples/X_train.csv', delimiter=',')
    y_train = np.loadtxt('./training_samples/Y_def_place_train.csv', delimiter=',')
    x_test = np.loadtxt('./training_samples/X_test.csv', delimiter=',')
    y_test = np.loadtxt('./training_samples/Y_def_place_test.csv', delimiter=',')

    # Транспонирование
    X_train = x_train.transpose()
    X_test = x_test.transpose()

    # Указание типа данных
    X_train = X_train.astype('float32')
    X_test = X_test.astype('float32')
    Y_train = y_train.astype(int)
    Y_test = y_test.astype(int)

    # Преобразуем метки в категории
    Y_train = np_utils.to_categorical(Y_train, 1000)
    Y_test = np_utils.to_categorical(Y_test, 1000)

    # 0 - > [1, 0]
    # 1 - > [0, 1]

    # Создаем последовательную модель
    model = Sequential()

    # Добавляем уровни сети
    model.add(Dense(1000, input_dim=1000, activation="relu", kernel_initializer="normal"))
    model.add(Dense(2000, activation="relu", kernel_initializer="normal"))
    model.add(Dense(1000, activation="relu", kernel_initializer="normal"))
    model.add(Dense(1000, activation="softmax", kernel_initializer="normal"))

    # Компилируем модель
    model.compile(loss="categorical_crossentropy", optimizer="SGD", metrics=["accuracy"])

    print(model.summary())

    # Обучаем сеть
    model.fit(X_train, Y_train, batch_size=200, epochs=10, validation_split=0.2, verbose=2)

    # Оцениваем качество обучения сети на тестовых данных
    scores = model.evaluate(X_test, Y_test, verbose=0)
    print("Точность работы на тестовых данных: %.2f%%" % (scores[1]*100))

    # Запускаем сеть на входных данных
    predictions = model.predict(X_test)

    # Преобразуем входные данные сети из категорий в метки классов (цифры от 0 - 9)
    temp = np.argmax(predictions, axis=1)

    print(temp[:25])
    print(y_test.astype(int)[:25])
    print(abs(y_test.astype(int)[:25]-temp[:25]))
    print(sum(abs(y_test.astype(int)[:]-temp[:])))


    # # USING model auto learning
    # # serialize model to JSON
    # model_json = model.to_json()
    # with open("./models/model.json", "w") as json_file:
    #     json_file.write(model_json)
    #
    # # serialize weights to HDF5
    # model.save_weights("./models/model.h5")
    # print("Saved model to disk")
    #
    # OR
    #
    # from keras.models import load_model
    #
    # model.save('./models/my_model.h5')  # creates a HDF5 file 'my_model.h5'
    # del model  # deletes the existing model
    #
    # # returns a compiled model
    # # identical to the previous one
    # model = load_model('./models/my_model.h5')