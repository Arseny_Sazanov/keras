# Свои библиотеки
from Lib import help_func as HP, LIB, MyPlot as MP

# Системные библиотеки
import argparse

# НАУЧНЫЕ БИБЛИОТЕКИ
import numpy as np
import matplotlib.pyplot as plt  # графика


# Инициализ
Logger = LIB.Logger  # Логгер


# Парсер аргументов командной строки
def create_parser():
    pars = argparse.ArgumentParser()
    pars.add_argument('-v', '--verbose', action='store_const', help="Debug mod (On/Off)", const=True,
                      default=False)  # Debug mod
    return pars

########################################################################################################################
# Подготовка данных для НС
# Н.У.
# Параметры системы (авторегрессии)


c0 = 0
a1 = 0  # 0.3
a2 = 0  # 0.5

# параметры случ процесса
M0 = 0
D0 = 1

# Параметры дефекта
M_def = 1
D_def = 1

NN_M = 10  # размер окна, число входных нейронов НС
NN_K = 1   # размер выхода, число выходных нейронов НС

T = 50  # NN_M*2 кол-во точек sample в эксперименте
K_train = 10000  # размер обучающей выборки
K_test = 10000   # размер контрольной выборки

K = K_test + K_train

# параметры расположения дефектов
MIN_PLACE_DEF = 40
MAX_PLACE_DEF = T

#                                                 M  A  I  N
if __name__ == '__main__':

    # Работа с аргументами командной строки
    parser = create_parser()
    namespace = parser.parse_args()  # ==== namespace = parser.parse_args(sys.argv[1:])


# ГЕНЕРАЦИЯ ПОСЛЕДОВАТЕЛЬНОСТЕЙ
    # Инициализация генератора случайных чисел, для воспроизведение результатов
    np.random.seed(42)

    # Генерация матрицы N*K случ чисел
    R0 = np.random.normal(M0, D0, (T, K_train+K_test))             # Без дефекта. Нормальное распределение Гаусса-Лапласа
    R_def = np.random.normal(M_def, D_def, (T, K_train+K_test))  # С дефектом. Нормльное распределение Гаусса-Лапласа
    # Для генерации дефектов
    # Def_happen = np.random.randint(0, 2, (K_train + K_test))     # Факт дефекта
    t_def = np.random.randint(MIN_PLACE_DEF, MAX_PLACE_DEF, (K_train + K_test))      # Место дефекта

    # print(R0)
    # print(R_def)

    # Уравнение авторегрессия второго порядка
    # Xn = c + a1 * Xn-1 + a2*Xn-2 + En  (En - шум случ. сост. с зада M и D)

    # Анализ времени генерации выборок
    with LIB.Profiler(namespace.verbose, "Time generate samples") as p_samples:
        # Создание статус бара для индикации
        status_bar = LIB.ProcessingCounter("[GENERATE SAMPLES] it is processing...",
                                           max_cnt_value=K * (T - NN_M),
                                           every_cnt_percent=1,
                                           silence=not namespace.verbose)
        temp = np.zeros((T, K))  # процессы с дефекто в момент t
        X = np.zeros((NN_M, K * (T - NN_M)))  # мини подвыборки по NN_MM элементов
        Y = np.zeros(K * (T - NN_M)).astype('int')          # учитель, есть дефект, нет дефекта
        for j in range(0, K):  # для каждого эксперимента делаем выборку
            for i in range(0, t_def[j]):
                temp[i][j] = c0 + R0[i][j]   # + a1 * temp[i-1][j] + a2 * temp[i-2][j]
                if i > 0:
                    temp[i][j] += a1 * temp[i-1][j]
                if i > 1:
                    temp[i][j] += a2 * temp[i-2][j]
            for i in range(t_def[j], T):
                temp[i][j] = c0 + R_def[i][j]  # + a1 * X[i-1][j] + a2 * X[i-2][j]

            for k in range(0, T - NN_M):
                status_bar.increment_counter()  # инкремент счетчик статус бара
                v = (T - NN_M) * j + k
                X[:, v] = temp[k:NN_M+k, j]
                Y[v] = 1 if NN_M+k >= t_def[j] else 0

    Logger.info("Window with defect: %s" % Y.sum(axis=0))
    Logger.info("Window without defect: %s" % (Y.size - Y.sum(axis=0)))
    Logger.info("Total cnt window: %s" % Y.size)

    with LIB.Profiler(namespace.verbose, "Save csv files") as p_save_csv:
        Logger.info("START SAVE DATA TO CSV FILES!")
        # Сохранение выборок в csv файлы
        # Выборки для обучения и тестирования НС
        np.savetxt('./training_samples/X_train.csv', X[:, 0:K_train*(T - NN_M)], fmt='%.18e', delimiter=',', newline='\n')
        Logger.info("./training_samples/X_train.csv SAVED!")
        np.savetxt('./training_samples/X_test.csv', X[:, K_train*(T - NN_M):], fmt='%.18e', delimiter=',', newline='\n')
        Logger.info("./training_samples/X_test.csv SAVED!")

        # Ответы учителя (есть дефект - 1, нет дефекта -0)
        np.savetxt('./training_samples/Y_train.csv', Y[0:K_train*(T - NN_M)], fmt='%1.0f', delimiter=',', newline='\n')
        Logger.info("./training_samples/Y_train.csv SAVED!")
        np.savetxt('./training_samples/Y_test.csv', Y[K_train*(T - NN_M):], fmt='%1.0f', delimiter=',', newline='\n')
        Logger.info("./training_samples/Y_test.csv SAVED!")

        # Массив для пост обработки дефектов (места дефектов)
        np.savetxt('./training_samples/Y_place_def_test.csv', t_def[K_train:], fmt='%4.0f', delimiter=',',
                   newline='\n')
        Logger.info("./training_samples/Y_place_def_test.csv SAVED!")

        Logger.info("FINISHED SAVE DATA TO CSV FILES!")


# Г Р А Ф И К А

    # Простой рисунок
    fig = plt.figure()
    # Добавление на рисунок прямоугольной (по умолчанию) области рисования
    #scatter1 = plt.scatter(0.0, 1.0)

    # Графики
    graph1 = plt.plot(np.arange(0, T, 1), temp[:, 0])
    plt.plot(np.ones(14)*t_def[0], np.arange(-4, 10, 1), color='red', linewidth=2)

    my_dict = {'color': 'red'}

    # Текст
    text1 = plt.text(t_def[0], -2.5, 'Момент разладки', **my_dict)

    # Сетка
    grid1 = plt.grid(True, color='black', linewidth=1.0)

    # Название графика
    plt.title('Autoregression 0 order')

    # Подписи осей
    plt.ylabel('Value')
    plt.xlabel('Sample numbers')

    # Сохранение графика в форматах: pdf, png
    MP.save(name='pic_test', fmt='pdf')
    MP.save(name='pic_test', fmt='png')

    # Показ графика
    plt.show()

    # Закрытие графика
    # plt.close()

