﻿# Свои библиотеки
from Lib import LIB

# ОБЩИЕ НАУЧНЫЕ БИБЛИОТЕКИ
import numpy as np
import matplotlib.pyplot as plt 
# %matplotlib inline   # for Jupyter Notebook

import pydot_ng as pydot
import numpy as np

# НЕЙРОСЕТЕВЫЕ БИБЛИОТЕКИ
from keras.datasets import mnist
from keras.models import Sequential
from keras.layers import Dense
from keras.utils import np_utils
from keras.utils import plot_model

from IPython.display import Image
from IPython.core.display import HTML 

Logger = LIB.Logger  # Логгер

# Параметры авторегресии 
c0 = 0
a1 = 0  # 0.3
a2 = 0  # 0.5

# Харакетеристики случайного процесса без дефекта
M0 = 0  # Мат. ожидание
D0 = 1  # Дисперсия

# Харакетеристики случайного процесса с дефектом
M_def_train = 1  # Мат. ожидание обучающей выборки
D_def_train = 1  # Дисперсия обучающей выборки

M_def_test = 5   # Мат. ожидание тестовой выборки
D_def_test = 1   # Дисперсия тестово выборки

# Параметры подвыборок
M = 10   # размер окна, число входных нейронов НС
T = 30   # NN_M*2 кол-во точек sample в эксперименте
N = T-M  # кол-во подвыборок для родительской выборки

# Диапозон возможного появления дефекта
MIN_PLACE_DEF = 15
MAX_PLACE_DEF = T

# Размеры выборок
K_train = 100000  # размер обучающей выборки
K_test = 100000   # размер контрольной выборки
K = K_test + K_train

# Настройки НС
INPUT_NN = 10    # размер вектора входа НС
OUTPUT_NN = 2   # размер вектора выхода НС

LAYERS_1_N = 10
activation_in = "relu"

activation_out = "softmax"
kernel_initializer = "normal"

loss = "categorical_crossentropy"
optimizer = "SGD"
metrics = ["accuracy"]

batch_size = 200
epochs = 10

# Инициализация генератора случайных чисел, для воспроизводимости результатов
np.random.seed(42)

# Генерация матриц cлучайных последовательностей
R0_train = np.random.normal(M0, D0, (T, K_train))  # Без дефекта. Тренировочное. Нормальное распределение Гаусса-Лапласа
R0_test = np.random.normal(M0, D0, (T, K_test))    # Без дефекта. Тестовое. Нормальное распределение Гаусса-Лапласа
R_def_train = np.random.normal(M_def_train, D_def_train, (T, K_train)) # С дефектом. Нормальное распределение Гаусса-Лапласа
R_def_test = np.random.normal(M_def_test, D_def_test, (T, K_test))     # С дефектом. Нормальное распределение Гаусса-Лапласа
# Для генерации дефектов
Def_happen = np.random.randint(0, 2, K_test)                       # Факт дефекта
t_def = np.random.randint(MIN_PLACE_DEF, MAX_PLACE_DEF, (K_train + K_test))      # Место дефекта

# Оценка качества распрделения места дефекта 
P_place_def = np.zeros(MAX_PLACE_DEF).astype('int')
for i in t_def:
    P_place_def[i] += 1

#Image(url="http://www.esta.spb.ru/images/jupyter/test_keras/Normal_distribution_pdf.png")

# Простой рисунок
fig1 = plt.figure()
# # Добавление на рисунок прямоугольной (по умолчанию) области рисования
# scatter1 = plt.scatter(0.0, 1.0)

# Графики
graph1 = plt.plot(np.arange(0, T, 1), R0_train[:, 0], label='Случайный процесс без дефекта M=%s D=%s' % (M0, D0));
graph2 = plt.plot(np.arange(0, T, 1), R_def_train[:, 0],  label='Дефект обучающий выборки M=%s D=%s' % (M_def_train, D_def_train));
graph3 = plt.plot(np.arange(0, T, 1), R_def_test[:, 0],  label='Дефект тестовой выборки M=%s D=%s' % (M_def_test, D_def_test));

# Сетка
grid1 = plt.grid(True, color='black', linewidth=1.0)

# Название графика
plt.title('Случайный процессы')
plt.legend()

# Подписи осей
plt.ylabel('Value')
plt.xlabel('Sample numbers')

# # Сохранение графика в форматах: pdf, png
# MP.save(name='test', fmt='pdf')
# MP.save(name='test', fmt='png')

# Показ графика
#plt.show()
plt.close(fig1)

# График распределения мест дефектов
fig2 = plt.figure()
plt.bar(np.arange(MAX_PLACE_DEF), P_place_def, align='center')

# Название графика
plt.title('График распределения мест дефектов')
plt.legend()

# Подписи осей
plt.ylabel('Count defects')
plt.xlabel('T')

#plt.show()
plt.close(fig2)


# Уравнение авторегрессия второго порядка
# Xn = c + a1 * Xn-1 + a2*Xn-2 + En  (En - шум случ. сост. с зада M и D)

temp = np.zeros((T, K))  # процессы с дефектом в момент t
X = np.zeros((M, K * N))  # мини подвыборки по NN_MM элементов
Y = np.zeros(K * N).astype('int')          # учитель, есть дефект, нет дефекта

status_bar = LIB.ProcessingCounter("[GENERATE TRAIN SAMPLES] it is processing...",
                                           max_cnt_value=K * N,
                                           every_cnt_percent=10,
                                           silence=False)

# По всем экспериментам
for j in range(K):  # для каждого эксперимента делаем выборку
    # Генерация участка без дефекта [0:t_def)
    for i in range(0, t_def[j]):
        temp[i][j] = c0 + R0_train[i][j] if j < K_train else R0_test[i][j-K_train]   # + a1 * temp[i-1][j] + a2 * temp[i-2][j]
        if i > 0:
            temp[i][j] += a1 * temp[i-1][j]
        if i > 1:
            temp[i][j] += a2 * temp[i-2][j]

    # Генерация участка с дефектом [t_def:T]
    for i in range(t_def[j], T):
        # Выбор дефекта в зависимости от выборки
        temp[i][j] = c0 + R_def_train[i][j] if j < K_train else R_def_test[i][j-K_train]  # + a1 * X[i-1][j] + a2 * X[i-2][j]

    # Генерация окошек из эксперимента [0:M], [1:M+1]  e.t.c.
    for k in range(0, T - M):
        status_bar.increment_counter()  # инкремент счетчик статус бара
        v = N * j + k
        X[:, v] = temp[k:M+k, j]
        Y[v] = 1 if M+k >= t_def[j] else 0


status_bar = LIB.ProcessingCounter("[GENERATE TEST SAMPLES] it is processing...",
                                           max_cnt_value=K_test * N,
                                           every_cnt_percent=10,
                                           silence=False)

temp = np.zeros((T, K_test))                 # временный массив
XX = np.zeros((M, K_test * N))             # мини подвыборки по NN_MM элементов
YY = np.zeros(K_test * N).astype('int')    # учитель, есть дефект, нет дефекта

# Генерация тестовой выборки (поседоватлеьности окошек) 50% c дектами, 50% без дефектов
for j in range(K_test):
    if Def_happen[j] == 0:  # without defect
        for i in range(0, T):
            temp[i][j] = c0 + R0_test[i][j]  # + a1 * temp[i-1][j] + a2 * temp[i-2][j]
            if i > 0:
                temp[i][j] += a1 * temp[i - 1][j]
            if i > 1:
                temp[i][j] += a2 * temp[i - 2][j]
    else:   # == 1 with defect
        # Генерация участка без дефекта
        for i in range(0, t_def[K_train+j]):
            temp[i][j] = c0 + R0_test[i][j]  # + a1 * temp[i-1][j] + a2 * temp[i-2][j]
            if i > 0:
                temp[i][j] += a1 * temp[i - 1][j]
            if i > 1:
                temp[i][j] += a2 * temp[i - 2][j]

        # Генерация участка с дефектом [t_def:T]
        for i in range(t_def[K_train+j], T):
            # Выбор дефекта в зависимости от выборки
            temp[i][j] = c0 + R_def_test[i][j]  # + a1 * X[i-1][j] + a2 * X[i-2][j]

    # Генерация окошек из эксперимента [0:M], [1:M+1]  e.t.c.
    for k in range(0, T - M):
        status_bar.increment_counter()  # инкремент счетчик статус бара
        v = N * j + k
        XX[:, v] = temp[k:M + k, j]
        YY[v] = 1 if Def_happen[j] == 1 and M + k >= t_def[K_train+j] else 0


W = np.zeros(2).astype("int")
W[0] = Y.sum(axis=0)
W[1] = Y.size - W[0]

Logger.info("Window with defect: %s" % W[0])
Logger.info("Window without defect: %s" % W[1])
Logger.info("Total cnt window: %s" % Y.size)

# График распрделения мест дефекта
fig3 = plt.bar(np.arange(2), W, align='center')
# Название графика
plt.title('График распределения мест дефектов')
plt.legend()

# Подписи осей
plt.ylabel('Count window')
plt.xticks(range(2), ['Дефект', 'Без дефекта'])
# plt.show()
# plt.close(fig3)


# Транспонирование
X_train = X[:, :K_train*N].transpose()
X_test = XX.transpose()

y_train = Y[:K_train*N]
y_test = YY

# Указание типа данных
X_train = X_train.astype('float32')
X_test = X_test.astype('float32')
Y_train = y_train.astype(int)
Y_test = y_test.astype(int)

# Преобразуем метки в категории
Y_train = np_utils.to_categorical(Y_train, 2)
Y_test = np_utils.to_categorical(Y_test, 2)

# 0 - > [1, 0]
# 1 - > [0, 1]

# Создаем последовательную модель
model = Sequential()

# Добавляем уровни сети
model.add(Dense(LAYERS_1_N, input_dim=INPUT_NN, activation=activation_in, kernel_initializer=kernel_initializer))
model.add(Dense(OUTPUT_NN, activation=activation_out, kernel_initializer=kernel_initializer))

# Компилируем модель
model.compile(loss=loss, optimizer=optimizer, metrics=metrics)

print(model.summary())

# Обучаем сеть
model.fit(X_train, Y_train, batch_size=batch_size, epochs=epochs, validation_split=0.2, verbose=2)

#plot_model(model, to_file='model.png', show_shapes=True)

# Оцениваем качество обучения сети на тестовых данных
scores = model.evaluate(X_test, Y_test, verbose=0)
print("Точность работы на тестовых данных: %.2f%%" % (scores[1]*100))

# Запускаем сеть на входных данных
predictions = model.predict(X_test)

# Преобразуем входные данные сети из категорий в метки классов (цифры от 0 - 9)
predictions = np.argmax(predictions, axis=1)

# Пост обработка: "Оценка скорости обнаружения дефекта (на каком такте) на РОДИТЕЛЬСКОЙ выборке"

Logger.info("\nPostprocessing START!\n")

y_place = t_def[K_train:]  # тестовые данные
y_fact = Def_happen        # тестовые данные факт дефекта

Err = []  # вспомогательный массив для дефектов
Err1 = 0  # дефект не найден, а он есть
Err2 = 0  # дефект найден, но его нет

V = (-100)*np.ones((y_place.size, N)).astype("int")  # скорость предсказания (-100 не было, +100 пропуск, >0 задержка тактов)

# Статистика по макро выборкам
K_found_late = np.zeros(y_place.size).astype("int")
K_found_zero = np.zeros(y_place.size).astype("int")
K_found_false = np.zeros(y_place.size).astype("int")
K_not_found_true = np.zeros(y_place.size).astype("int")
K_not_found_false = np.zeros(y_place.size).astype("int")

Err = predictions - y_test  # Подсчет процентов верных обнаруж, ошибок 1 и 2 рода в окнах

Cnt_def = y_fact.sum(axis=0)
Cnt_without_def = y_fact.size - Cnt_def


KK = {}

for k in range(y_fact.size):
    for n in range(N):
        i = k*N + n

        if predictions[i] > 0:  # сеть нашла дефект
            if y_fact[k] == 0:  # в макро выборке нет дефекта, сеть нашла ложный дефект
                K_found_false[k] = 1
                K_not_found_false[k] = 0
                K_not_found_true[k] = 0
                break  # TODO THINK самовостанавливающиеся дефекты!
            else:
                V[k][n] = (M + n - y_place[k])  # вычисляем на сколько тактов она запоздала/опередила/угадала
                if V[k][n] == 0:  # если = 0 сеть точно нашла место дефекта
                    K_found_zero[k] = 1
                    K_not_found_false[k] = 0
                    K_not_found_true[k] = 0
                    break  # TODO THINK самовостанавливающиеся дефекты!
                if V[k][n] > 0:  # если > -0 - сеть запоздала но нашла дефект
                    K_found_late[k] = 1  # установка флажка что дефект обнаружен в данной макро выборке
                    K_not_found_false[k] = 0
                    K_not_found_true[k] = 0
                    break  # TODO THINK самовостанавливающиеся дефекты!
                if V[k][n] < 0:  # если < 0 - сеть нашла ложный дефект
                    K_found_false[k] = 1
                    K_not_found_false[k] = 0
                    K_not_found_true[k] = 0
                    break  # TODO THINK самовостанавливающиеся дефекты!
        else:  # сеть дефект не нашла,
            if y_test[i] > 0:  # но дефект есть :(, установка значения 100 - пропуск дефекта
                V[k][n] = 100
                K_not_found_false[k] = 1
                K_not_found_true[k] = 0  # TODO think самовостанавливающиеся дефекты!
            else:  # и дефекта нет
                K_not_found_true[k] = 1
                K_not_found_false[k] = 0  # TODO think самовостанавливающиеся дефекты!

Err1 = sum([1 if v > 0 else 0 for v in Err]) / predictions.size  # ошибка 1 рода по всем окнам
Err2 = sum([1 if v < 0 else 0 for v in Err]) / predictions.size  # ошибка 2 рода по всем окнам
Rigth = sum([1 if v == 0 else 0 for v in Err]) / predictions.size  # верно по всем окнам

# Logger.info("K_found_zero:      %s\n"
#             "K_found_late:      %s\n"
#             "K_found_false:     %s\n"
#             "K_not_found_false: %s\n"
#             "K_not_found_true:  %s\n"
#             % (K_found_zero, K_found_late, K_found_false, K_not_found_false, K_not_found_true))

K_found_zero = K_found_zero.sum(axis=0)            # / K_found_zero.size
K_found_late = K_found_late.sum(axis=0)            # / K_found_zero.size
K_found_false = K_found_false.sum(axis=0)          # / K_found_zero.size
K_not_found_false = K_not_found_false.sum(axis=0)  # / K_found_zero.size
K_not_found_true = K_not_found_true.sum(axis=0)    # / K_found_zero.size


V_sred = 0
temp_k = 0
for k in range(y_place.size):
    for n in range(0, N):
        if V[k][n] >= 0 and V[k][n] != 100:
            if V[k][n] not in KK.keys():
                KK[V[k][n]] = 1
            else:
                KK[V[k][n]] += 1
            V_sred += V[k, n]
            temp_k += 1
            break
V_sred = V_sred / temp_k

Logger.info("Macro with defect: %s\n without defect: %s\n" % (Cnt_def, Cnt_without_def))

Logger.info("Window with defect: %s" % W[0])
Logger.info("Window without defect: %s" % W[1])
Logger.info("Total cnt window: %s" % Y.size)

Logger.info("All windows: \n"
            "Rigth: %.2f%%\n"
            "Err1:  %.2f%%\n"
            "Err2:  %.2f%%\n" % (Rigth*100, Err1*100, Err2*100))

Logger.info(KK)

Logger.info("K_found_zero:      %s   %s%%\n"
            "K_found_late:      %s   %s%%\n"
            "K_found_false:     %s   %s%%\n"
            "K_not_found_false: %s   %s%%\n"
            "K_not_found_true:  %s   %s%%\n"
            % (K_found_zero, K_found_zero/y_fact.size*100,
               K_found_late, K_found_late/y_fact.size*100,
               K_found_false, K_found_false/y_fact.size*100,
               K_not_found_false, K_not_found_false/y_fact.size*100,
               K_not_found_true, K_not_found_true/y_fact.size*100))


Logger.info("V_rigth_cnt_find [Кол-во ВЕРНЫХ обнаружений/(и не обнаружений)]: {good}/{all}  {pT}\n"
            "V_false_cnt_find [Кол-во НЕВЕРНЫХ обнаружений/(и ложных обнаружений)]: {bad}/{all}  {pF}\n"
            "V_min_sred_rigth [Мин кол-во тактов до правильного обнаружения]: {v}\n"
            .format(good=K_found_zero + K_found_late + K_not_found_true,
                    bad=K_found_false + K_not_found_false,
                    all=y_fact.size,
                    pT=(K_found_zero + K_found_late + K_not_found_true) / y_fact.size * 100,
                    pF=(K_found_false + K_not_found_false) / y_fact.size * 100,
                    v="%.2f" % V_sred)
            )
print(V)






