import keras
from Lib import LIB
from keras.models import Sequential
from keras.layers import Dense, Dropout, Flatten, Activation
from keras.layers import Conv2D, MaxPooling2D
from keras.layers import Conv1D, GlobalAveragePooling1D, MaxPooling1D
from keras import backend as BackendKeras
import re

def generate_model(NameModel, MS):
    '''
     General Generator Keras Model
    :param NameMode:
    :param MS: Settings Keras Models
    :return:  Keras model
    '''

    model = None
    if re.match("FFNN", NameModel):
        model = generate_FFNN(MS)
    elif re.match("CNN", NameModel):
        model = generate_CNN(MS)
    elif re.match("1D_CNN", NameModel):
        model = generate_CNN(MS)
    else:
        pass

    return model


def generate_FFNN(MS):
    '''
    Generator FFNN Keras model
    :param MS: Settings of Keras Model
    :return: Keras model
    '''

    # Создаем последовательную модель
    model = Sequential()

    # Добавляем слои

    # 1. Входной слой
    model.add(dense_layer(MS["INPUT_LAYER"]))

    # 2. Скрытые слои
    for i, Layer in enumerate(MS["HIDDEN_LAYERS"]):
        model.add(dense_layer(Layer))

    # 3. Выходной слой
    model.add(dense_layer(MS["OUTPUT_LAYER"]))

    return model


# TODO
def generate_CNN(MS):
    '''
    Generator CNN Keras model
    :param S: Settings of Keras Model
    :return: Keras model
    '''

    model = None
    if MS["api_mode"] == "Functional":  # Function API MODE
        pass  # TODO Function API
    else:  # Последовательная модель
        model = Sequential()
        # Добавляем слои
        try:
            for i, L in enumerate(MS["LAYERS"]):
                if L["name"] == "conv1d":
                    if "input_shape" in L.keys():
                        model.add(
                            Conv1D(
                                L["nb_filters"],
                                kernel_size=L["kernel_size"],
                                activation=L["activation"],
                                padding=L["padding"],
                                input_shape=MS["input_shape"])
                        )
                    else:
                        model.add(
                            Conv1D(
                                L["nb_filters"],
                                kernel_size=L["kernel_size"],
                                padding=L["padding"],
                                activation=L["activation"])
                        )
                elif L["name"] == "conv2d":
                    if "input_shape" in L.keys():
                        model.add(
                            Conv2D(
                                L["nb_filters"],
                                kernel_size=(L["kernel_size"], L["kernel_size"]),
                                activation=L["activation"],
                                padding=L["padding"],
                                input_shape=MS["input_shape"])
                        )
                    else:
                        model.add(
                            Conv2D(
                                L["nb_filters"],
                                kernel_size=(L["kernel_size"], L["kernel_size"]),
                                padding=L["padding"],
                                activation=L["activation"])
                        )
                elif L["name"] == "dense":
                    model.add(Dense(L["units"], activation=L["activation"]))
                elif L["name"] == "maxpooling1d":
                    model.add(MaxPooling1D(L["kernel_size"]))
                elif L["name"] == "maxpooling2d":
                    model.add(MaxPooling2D((L["kernel_size"], L["kernel_size"])))
                elif L["name"] == "flatten":
                    model.add(Flatten())
                elif L["name"] == "dropout":
                    model.add(Dropout(L["value"]))
                else:
                    LIB.CLogger.error("Err value of name param. Value: %s " % L["name"])
        except Exception as inst:
            LIB.CLogger.error("Err while forming CNN %s " % inst)
            model = None

    # # TODO!!!!
    # model = Sequential()
    # model.add()  # channel last TODO
    # model.add(MaxPooling2D(pool_size=(2, 2)))
    # model.add(Dropout(0.25))
    # model.add(Flatten())
    # model.add(Dense(128, activation='relu'))
    # model.add(Dropout(0.5))
    # model.add(Dense(2, activation='softmax'))

# LAST MODEL
    # # LIKE CIFAR-10
    # model = Sequential()
    # model.add(Conv2D(32, (3, 3), padding='same',
    #                  input_shape=MS["input_shape"], activation='relu'))
    # # model.add(Conv2D(32, (3, 3), activation='relu', padding='same'))
    # model.add(Dropout(0.2))
    # model.add(MaxPooling2D(pool_size=(2, 2)))
    #
    # # model.add(Conv2D(32, (2, 2), activation='relu', padding='same'))
    # # model.add(Dropout(0.2))
    # # model.add(MaxPooling2D(pool_size=(2, 2)))
    #
    # # model.add(Dropout(0.2))
    # # model.add(Conv2D(128, (2, 2), padding='same', activation='relu'))
    # # model.add(Conv2D(64, (3, 3), activation='relu'))
    # # model.add(MaxPooling2D(pool_size=(2, 2)))
    # # model.add(Dropout(0.25))
    #
    # model.add(Flatten())
    # model.add(Dense(256, activation='relu'))
    # model.add(Dropout(0.5))
    # model.add(Dense(2, activation='softmax'))
##
    # model = Sequential()
    # model.add(Conv2D(32, kernel_size=(5, 5), strides=(1, 1),
    #                  activation='relu',
    #                  input_shape=MS["input_shape"]))
    # model.add(MaxPooling2D(pool_size=(2, 2), strides=(2, 2)))
    # model.add(Conv2D(64, (5, 5), activation='relu'))
    # model.add(MaxPooling2D(pool_size=(2, 2)))
    # model.add(Flatten())
    # model.add(Dense(1000, activation='relu'))
    # model.add(Dense(2, activation='softmax'))


    # model = Sequential()
    # model.add(Conv2D(32, kernel_size=(3, 3),
    #                  activation='relu',
    #                  input_shape=MS["input_shape"]))
    # model.add(Conv2D(64, (3, 3), activation='relu'))
    # model.add(MaxPooling2D(pool_size=(2, 2)))
    # model.add(Dropout(0.25))
    # model.add(Flatten())
    # model.add(Dense(128, activation='relu'))
    # model.add(Dropout(0.5))
    # model.add(Dense(2, activation='softmax'))

    return model

# def 1D_CNN(MS):
#     '''
#        Generator 1D_CNN Keras model
#        :param S: Settings of Keras Model
#        :return: Keras model
#        '''
#
#     model = Sequential()
#     model.add(Conv1D(32, 3, activation='relu', input_shape=MS["input_shape"]))
#     # model.add(Conv1D(32, 3, activation='relu'))
#     model.add(MaxPooling1D(2))
#     model.add(Dropout(0.25))
#
#     # model.add(Conv1D(64, 3, activation='relu'))
#     # model.add(Conv1D(64, 3, activation='relu'))
#     # model.add(Conv1D(128, 3, activation='relu'))
#     # model.add(MaxPooling1D(2))
#
#     # model.add(Conv1D(256, 3, activation="relu"))
#     # model.add(Conv1D(256, 3, activation="relu"))
#     # model.add(Conv1D(256, 3, activation="relu"))
#     # model.add(MaxPooling1D(3))
#     model.add(Flatten())
#     model.add(Dense(128, activation="relu"))
#     # model.add(GlobalAveragePooling1D())
#     # model.add(Dropout(0.5))
#     model.add(Dense(2, activation='softmax'))
#
#     return model


def prepare_dense_layer_settings(MSL):
    '''
    :param MSL: Настройка слоя
    Arguments
        units: Positive integer, dimensionality of the output space.
        activation: Activation function to use (see activations). If you don't specify anything, no activation is applied (ie. "linear" activation: a(x) = x).
        use_bias: Boolean, whether the layer uses a bias vector.
        kernel_initializer: Initializer for the kernel weights matrix (see initializers).
        bias_initializer: Initializer for the bias vector (see initializers).
        kernel_regularizer: Regularizer function applied to the kernel weights matrix (see regularizer).
        bias_regularizer: Regularizer function applied to the bias vector (see regularizer).
        activity_regularizer: Regularizer function applied to the output of the layer (its "activation"). (see regularizer).
        kernel_constraint: Constraint function applied to the kernel weights matrix (see constraints).
        bias_constraint: Constraint function applied to the bias vector (see constraints).
    :return: MSL - настройка слоя дополненная настройками по умолчанию
    '''

    MSL["activation"] = None if "activation" not in MSL.keys() else MSL["activation"]
    MSL["use_bias"] = True if "use_bias" not in MSL.keys() else MSL["use_bias"]
    MSL["kernel_initializer"] = 'glorot_uniform' if "kernel_initializer" not in MSL.keys() else MSL["kernel_initializer"]
    MSL["bias_initializer"] = 'zeros' if "bias_initializer" not in MSL.keys() else MSL["bias_initializer"]
    MSL["kernel_regularizer"] = None if "kernel_regularizer" not in MSL.keys() else MSL["kernel_regularizer"]
    MSL["bias_regularizer"] = None if "bias_regularizer" not in MSL.keys() else MSL["bias_regularizer"]
    MSL["activity_regularizer"] = None if "activity_regularizer" not in MSL.keys() else MSL["activity_regularizer"]
    MSL["kernel_constraint"] = None if "kernel_constraint" not in MSL.keys() else MSL["kernel_constraint"]
    MSL["bias_constraint"] = None if "bias_constraint" not in MSL.keys() else MSL["bias_constraint"]

    return MSL


def dense_layer(MSL):
    '''

    :param MSL: Настройка слоя
    :return: DENSE Keras Layer
    '''
    PMSL = prepare_dense_layer_settings(MSL)  # Дополнение отсутствующих настроек по умолчанию
    if "input_dim" not in PMSL.keys():
        return Dense(
            MSL["units"],
            activation=PMSL["activation"],
            use_bias=PMSL["use_bias"],
            kernel_initializer=PMSL["kernel_initializer"],
            bias_initializer=PMSL["bias_initializer"],
            kernel_regularizer=PMSL["kernel_regularizer"],
            bias_regularizer=PMSL["bias_regularizer"],
            activity_regularizer=PMSL["activity_regularizer"],
            kernel_constraint=PMSL["kernel_constraint"],
            bias_constraint=PMSL["bias_constraint"])
    else:
        return Dense(
                    MSL["units"],
                    input_dim=PMSL["input_dim"],
                    activation=PMSL["activation"],
                    use_bias=PMSL["use_bias"],
                    kernel_initializer=PMSL["kernel_initializer"],
                    bias_initializer=PMSL["bias_initializer"],
                    kernel_regularizer=PMSL["kernel_regularizer"],
                    bias_regularizer=PMSL["bias_regularizer"],
                    activity_regularizer=PMSL["activity_regularizer"],
                    kernel_constraint=PMSL["kernel_constraint"],
                    bias_constraint=PMSL["bias_constraint"])
